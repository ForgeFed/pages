<!doctype html>
<html class="default">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ForgeFed</title>
    <link rel="stylesheet" type="text/css" href="/theme.css" />
    <link rel="icon" href="/img/logo.svg" />
    <script>
      // check whether the default colour scheme has been overridden
      // must be done as early as possible to avoid FOUC
      let theme = sessionStorage.getItem("theme")
      if (theme) document.documentElement.classList.add(theme)
    </script>
    <link rel="alternate" type="application/atom+xml" title="ForgeFed Blog" href="/atom.xml">
  </head>
  <body class="body">
    <header class="header">
      <h1 class="header-title">
        <a class="header-title__link" href="/">
            <img src="/img/logo-with-name.svg" alt="ForgeFed" title="ForgeFed" />
        </a>
      </h1>
      <nav class="nav" id="site-nav">
        <a class="nav__link"
           href="/spec">📓 specification</a>
        <a class="nav__link"
           href="/blog">📜 blog</a>
        <a class="nav__link"
           href="https://codeberg.org/ForgeFed/ForgeFed/issues">🐞 issues</a>
        <a class="nav__link"
           href="https://socialhub.activitypub.rocks/c/software/forgefed/60">🗒️ forum</a>
        <a class="nav__link"
           href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">💬 chat</a>
      </nav>
    </header>
    <main class="main">
      
  <h1 class="main-title">Stabilizing the Object Capability System</h1>
  <p>
    2023-06-09 by Pere Lev
  </p>
  <p>The previous blog post discussed the actor system refactoring work. I've built
into Vervis the foundations for actor programming, and started porting Activity
handlers to this new system.</p>
<p>Then, I moved on to my next task, which is about stabilizing ForgeFed's Object
Capability system.</p>
<h2 id="forgefed-stability">ForgeFed Stability</h2>
<p>The ForgeFed specification has been evolving for a few years now. Much less
implementation work happened, than I hoped, but the spec still made progress,
and so did the 2 primary implementations: Forgejo and Vervis.</p>
<p>During my work implementing the spec in Vervis, I've more and more noticed the
limitations of ActivityPub (on which ForgeFed in based), and I also had long
periods of time in which I took a pause from development work, due to feeling
the impact of the many-hours-at-the-screen and wanting a more people-focused
and less computer-focused life. These things raised a big question for me:
What's my place in the future of ForgeFed?</p>
<p>Despite the challenges, I still felt (and still feel) that passion in me, to
build the foundations for humanity to develop software in an environment that's
about using and developing technology in ways aimed at serving human needs and
holding the whole. Being in the Free Software Community for such a long time,
and seeing the tragic situation with github centralization and people's need
for a new solution, have given me energy to continue this important work.</p>
<p>A big part of the current work plan is also related to funding: I have funding
from NLNet for a year (which started in March), and the work plan is a
combination of my initial draft and the requests and advice of NLNet.</p>
<p>My goals for the funded work are:</p>
<ol>
<li>Create an initial proof-of-concept that implementes a federated application
based on actor programming rather than ActivityPub</li>
<li>Stabilize the core of the ForgeFed specification, without adding whole new
features, make the big decisions that haven't been made</li>
<li>Finish implementing that core in Vervis and turning it into a tool for
testing other implementations</li>
</ol>
<p>As the overall direction, I'll be shifting my focus from ActivityPub to
actor-programming, which I believe is a much better foundation for federated
forges, and for federated applications in general. However, in 2023 I'll still
be putting most of my energy into the ActivityPub-based work, bringing the big
open loops to conclusion.</p>
<p>One of these big open loops is the Object Capability system.</p>
<h2 id="why-object-capabilities">Why Object Capabilities</h2>
<p>ActivityPub doesn't have object capabilities built-in. And it's not obvious
whether and how to add them, in a way that's aligned with ActivityPub's design.
Could we, for simplicity, do without them, and use plain old ACLs and RBAC
instead?</p>
<p>For directly issued access, we sort of could. I'm saying <em>sort of</em>, because it
would cause the problem of &quot;ambient authority&quot; - merely being on an ACL is
enough to do some access to protected resources, without having to explicitly
own and provide any authorization token for the specific operation. However, if
we let go of solving that problem in ForgeFed (it's not like the whole web is
based on capabilities anyway), accessing a resource that directly granted you
access to it would be fairly easy.</p>
<p>The challenge comes when dealing not with a single resource, but with a
federated network of related resources that can contain and be contained in
other, possible remote resources. For example:</p>
<ul>
<li>Project <em>MyProject</em> contains a sub-project <em>MySubProject</em>, which contains a
Pull Request tracker <em>MyPRs</em> linked with a Git repo <em>MyRepo</em></li>
<li>There's a team <em>CoolTeam</em> that's managing <em>MyProject</em></li>
<li>I'm a member of <em>CoolTeam</em> wanting to merge a PR that someone submitted to
<em>MyMRs</em></li>
</ul>
<p>If I contact <em>MyPRs</em>, asking to merge PR #123, how does <em>MyPRs</em> know that I'm
authorized? It needs to detect that:</p>
<ol>
<li><em>MyPRs</em> is contained in <em>MySubProject</em>, so anyone with access to
<em>MySubProject</em> can also access <em>MyPRs</em></li>
<li><em>MySubProject</em> is in turn contained in <em>MyProject</em>, so if I had access to
<em>MyProject</em>, I'd be able to access <em>MyPRs</em></li>
<li><em>CoolTeam</em> is managing <em>MyProject</em>, so if I were either in <em>CoolTeam</em> itself
or in any of its subteams, I could access <em>MyPRs</em></li>
</ol>
<p>And indeed I'm a member of <em>CoolTeam</em>, so I expect <em>MyPRs</em> to approve my
request to merge PR #123. But how does <em>MyPRs</em> detect that chain of related
resources that links between <em>MyPRs</em> and me, given that all of these resources
may live on different servers?</p>
<p>That's where Object Capabilities shine: They a <em>flexible</em> tool that easily
supports <em>distributed</em> authorization:</p>
<ul>
<li><em>MyPRs</em> contacts <em>MySubProject</em>, saying: Here's an access token for me. Since
I'm contained in you, give this token to anyone who has access to you, so
that they can access me as well.</li>
<li><em>MySubProjet</em> in turn contacts <em>MyProject</em>, passing a token as well</li>
<li><em>MyProject</em> similarly passes a token to <em>CoolTeam</em></li>
<li><em>CoolTeam</em> passes a token to me</li>
</ul>
<p>Now, when I want to merge PR #123, I pass my token to <em>MyPRs</em>. And <em>MyPRs</em> can
verify the chain of tokens and be sure my access to <em>MyPRs</em> is approved at all
the links in the chain.</p>
<h1 id="representing-object-capabilities">Representing Object Capabilities</h1>
<p>ActivityPub doesn't have any standard support for object capabilities.
Actually, it doesn't even distinguish between commands and events, and uses
<em>descriptions</em> of events to issue commands, rather than referring to
explicitly-declared methods of actors. I'm guessing ActivityPub is primarily
made for publishing personal objects (notes, images, videos, etc.) and personal
events (read, like, eat, etc.), and not for general complex interaction logc
between remote objects. ForgeFed doesn't fit the personal-publishing picture,
since it's about collaborative resources such as issues, PRs, repos and teams.
But given the current situation where ForgeFed is based on ActivityPub, how do
we represent Object Capabilities?</p>
<p>When I first asked myself this question, and read the available material and
ideas about adding OCAPs to the Fediverse, I realized most of it was about C2S
and not S2S, and that mostly just visionary ideas and rough experiments were
available. Anything beyond that was lying in the realm of actor-programming, a
whole new dream for distributed application architecture and development.</p>
<p>So I went for a custom design for OCAPs, using a combination of ActivityPub
activities and new ForgeFed activities:</p>
<ul>
<li>Object capabilities are granted using a newly defined <code>Grant</code> activity, whose
<code>id</code> is used for invoking the capability</li>
<li>People can ask to add members to repos/projects/etc. using <code>Invite</code> and
<code>Join</code> activities, and if the request is approved, the resource sends a
<code>Grant</code></li>
<li><code>Grant</code>s can &quot;delegate&quot; other <code>Grant</code>s, i.e. extend the chain by adding a new
link</li>
</ul>
<p>This is a design of a kind I called <strong>representational</strong> in the previous blog
post. So it's not ideal. But given the design of ActivityPub and wanting to
design something that doesn't diverge from it, I decided it's reasonable.</p>
<p>Since then, I really hoped someone else, some other projects, would want or
need OCAPs, and a standard OCAP system for the Fediverse would emerge. I also
looked into other Fediverse projects, to see if anyone needed OCAPs. But I
couldn't find anything. It seemed that Fediverse apps are mostly in the field
of personal content publishing and sharing, and ForgeFed is almost alone trying
to use ActivityPub for collaborative resource access and complex distributed
authorization.</p>
<p>But still, in my funded work plan, I put a task: Check out existing OCAP
systems, and see if I can adapt ForgeFed's system to be compatible with them.
It could potentially save a lot of work for implementors, being able to use a
ready OCAP implementation instead of implementing a custom one from scratch.</p>
<h2 id="existing-object-capability-systems">Existing Object Capability Systems</h2>
<p>I found 2 systems that seem relevant:</p>
<ul>
<li><a href="https://ucan.xyz">UCAN</a></li>
<li><a href="https://w3c-ccg.github.io/zcap-spec">ZCAP</a></li>
</ul>
<p>Benefits over the custom system:</p>
<ul>
<li>Ability to embed delegations instead of just linking to them</li>
<li>Existing UCAN implementations in both Go (for Forgejo) and Haskell (for
Vervis)</li>
<li>Capabilities are cryptoraphically signed, which allows to verify their
authenticity using the signature rather than relying on <code>HTTP GET</code>ing the
<code>id</code> URI</li>
</ul>
<p>That made UCANs quite appealing! But when I looked deeper, I also felt that
the following factors have an impact.</p>
<p>For ZCAPs:</p>
<ul>
<li>ZCAP, while evolving over the years, is still a draft, still changing, and
still relies on Linked Data signatures, which are a big burden, requiring an
implementation of complicated JSON-LD algorithms</li>
<li>ZCAP doesn't an a vocabulary for methods and invocations of them, so I'd
still have to custom-define that for ForgeFed</li>
<li>No ready-to-use ZCAP implementations anyway</li>
</ul>
<p>And for UCANs:</p>
<ul>
<li>UCANs have a separate spec for invocation, which would mean diverging from
ActivityPub-based invocation, making ForgeFed quite incompatible with the
rest of the Fediverse</li>
<li>UCANs rely on DIDs, but the Fediverse isn't using decentralized IDs, so we'd
need to pick and implement a DID method relevant for the Fediverse, such as
<code>did:web</code></li>
<li>UCANs can be used without the invocation part, but that decreases the benefit
of reusing them, especially since we need our own DID</li>
<li>Fission, the company which seems to be leading the UCAN project, has
implemented its software in Haskell, but the Haskell implementation of UCANs
seems minimal and just for what Fission needs, e.g. by supporting only
<code>did:key</code>, and the code hasn't been updated for 10 months</li>
</ul>
<p>My primary concern is that switching to UCANs, but still needing to add extras
and adaptations, and a whole new JWT-based representation, would significantly
increase the ForgeFed specification's complexity. The custom system, while
being custom, seems simpler to me, and based purely on Activities, which is
very much in line with being an ActivityPub-based protocol.</p>
<p>What now?</p>
<h2 id="adapting-the-custom-system">Adapting the Custom System</h2>
<p>I decided that instead of using ZCAPs or UCANs, I'll try to <em>tweak</em> the custom
system to include the features and benefits that other systems have, and settle
on a stable complete definition that ForgeFed and its implementation can safely
depend on.</p>
<p>Here are some powers and benefits of OCAP systems out there (not just
ZCAP/UCAN):</p>
<ul>
<li>OCAPs have a start time and an expiration time</li>
<li>OCAPs can be cryptographically signed and verified, and therefore embedded</li>
<li>Revocations can refer to OCAPs by embedding them, because they're signed</li>
<li>Revocations are done by spreading revocation messages in the network, instead
of using live URIs</li>
<li>Promises, i.e. asynchronously sending the return value of a command sent
earlier</li>
<li>Immediate return values beyond success-or-failure HTTP status codes</li>
<li>Promise pipelining, i.e. sending method calls against actors and parameters
that haven't been created/computed/delivered yet, for network and programming
efficiency</li>
</ul>
<p>For the first 4 items, I've proposed updates to the ForgeFed specification:</p>
<ol>
<li>Allow <code>Grant</code>s to specify a <code>startTime</code> and <code>endTime</code></li>
<li>Use JSON-based object signatures to allow signing activities without relying
on JSON-LD (thanks to silverpill defining a
<a href="https://codeberg.org/fediverse/fep/src/branch/main/feps/fep-8b32.md">FEP</a>
for that)</li>
<li>Revocation messages may be a necessity in a fully p2p system, but a big
complication for systems like the Fediverse which do have live URIs, so I've
proposed to keep using live URIs for revocation but allow a <em>time buffer</em> to
prevent these URIs from being checked unnecessarily often (e.g. I don't
think more than once-a-second would be needed in most cases, maybe even
once-a-minute)</li>
</ol>
<p>At the time of writing, the PRs are still in review:
<a href="https://codeberg.org/ForgeFed/ForgeFed/pulls/197">#197</a>
<a href="https://codeberg.org/ForgeFed/ForgeFed/pulls/198">#198</a>,
<a href="https://codeberg.org/ForgeFed/ForgeFed/pulls/199">#199</a>.</p>
<p>In the meantime, I've implemented these changes in Vervis, including progress
porting the OCAP activity handlers to the new actor system:</p>
<ul>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/a22aeb85d09b9207f2d1f3ac1ca546fbd319be51">Optional duration buffer for Grant
revocation</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/ba02d62eb5b4ceb5cd871463f714a4d5b15f86f6">Grant start &amp; end times</a></li>
<li>Object integrity proof
<a href="https://codeberg.org/ForgeFed/Vervis/commit/e8e587af26944d3ea8d91f5c47cc3058cf261387">generation</a>
and
<a href="https://codeberg.org/ForgeFed/Vervis/commit/621275e25762a1c1e5860d07a6ff87b147deed4f">verification</a></li>
<li><code>Deck</code> (i.e. issue tracker actor) handlers:
<a href="https://codeberg.org/ForgeFed/Vervis/commit/9955a3c0ad7f459b1579e1a2fe61c6cb663a9a7c">Accept-Reject-Follow-Undo</a>,
<a href="https://codeberg.org/ForgeFed/Vervis/commit/85f77fcac47b1fbedfabe7f87d9383c92a5deef5">Invite</a>,
<a href="https://codeberg.org/ForgeFed/Vervis/commit/59e99f405adc862d253e7da819cadeaf29b380c7">Join</a></li>
<li>(Mostly trivial) <code>Person</code> handlers:
<a href="https://codeberg.org/ForgeFed/Vervis/commit/4d8e5de8b82ce12d1701d43cce50dd90748d7970">Invite</a>,
<a href="https://codeberg.org/ForgeFed/Vervis/commit/b759b87d0f0c21c920df83033c44fbaae1e8e229">Join</a>,
<a href="https://codeberg.org/ForgeFed/Vervis/commit/d467626049c814b9b90c77e45ba444177e0b61b5">Revoke</a></li>
</ul>
<h2 id="beyond-activitypub">Beyond ActivityPub</h2>
<p>So, 4 items from the OCAP-features list are taken care of. What remains is:</p>
<ul>
<li>Promises</li>
<li>Return values</li>
<li>Promise pipelining</li>
</ul>
<p>It's technically possible to extend ActivityPub to support these things, but
they also represent a significant shift from the (perhaps too) simple activity
model that standard ActivityPub has. And they involve some non-trivial
implementation work, especially promise pipelining. The approach I'm taking is
to implement the ActivityPub-based ForgeFed without those tools, and leaving
them for the more thorough actor-programming systems (such as Spritely and
Cap'n Proto) where such tools are available.</p>
<p>In parallel to the ActivityPub-based work, I'm already evolving an actor
programming system, step by step, and intending to use it for the &quot;Capybara
experiment&quot; AKA actor-system based Fediverse proof-of-concept.</p>
<p>Considering that Spritely is already working on the same thing, and that Cap'n
Proto has a Haskell implementation - am I wasting my time here? Hard (for me)
to say. I just know I'm very excited about actor programming, and that it's
still an evolving technology, and Spritely is only in Scheme right now, so a
Haskell library for actor programming is a valuable addition to the picture.
Cap'n Proto is a lower-level tool, but perhaps my system can wrap Cap'n Proto
or serve as inspiration for a higher-level API to use with Cap'n Proto.</p>
<p>And perhaps I can get a grant to fund my work on actor programming? And perhaps
more people will want to contribute? And perhaps a plugin system can allow
writing actors in different languages into the same application? We'll see :)</p>
<p>Landing back to the current focus: Stabilizing the core of the
ActivityPub-based ForgeFed.</p>
<h2 id="comments">Comments</h2>
<p>We have an account for ForgeFed on the Fediverse:
<a href="https://floss.social/@forgefed">https://floss.social/@forgefed</a></p>
<p>Right after publishing this post, I'll make a toot there to announce the post,
and you can comment there :)</p>


    </main>
    <footer class="footer">
      <p xmlns:dct="http://purl.org/dc/terms/">
        <a rel="license"
           href="http://creativecommons.org/publicdomain/zero/1.0/">
          <img src="https://licensebuttons.net/p/zero/1.0/88x31.png"
               style="border-style: none;"
               alt="CC0" />
        </a>
        <br />
        <a rel="dct:publisher" href="https://forgefed.org/">
          <span property="dct:title">The ForgeFed team</span>
        </a>
        has dedicated all copyright and related and neighboring
        rights to
        <span property="dct:title">ForgeFed</span> to the public domain
        worldwide.
      </p>
      <p>❤ Copying is an act of love. Please copy, reuse and share!</p>
      <p>
        Site generated with
        <a class="footer__link" href="https://www.getzola.org">Zola</a>
        and
        <a class="footer__link" href="https://tabatkins.github.io/bikeshed">Bikeshed</a>.
      </p>
      <p>
        <a href="https://liberapay.com/ForgeFed/donate">
          <img alt="Donate using Liberapay"
              src="https://liberapay.com/assets/widgets/donate.svg" />
        </a>
        <a href="https://opencollective.com/forgefed">
          <img alt="Donate using Open Collective"
              src="/img/open_collective.svg" />
        </a>
      </p>
    </footer>
    <script>
      let html = document.documentElement
      let siteNav = document.getElementById("site-nav")

      function currentColorScheme() {
        if (html.classList.contains("dark")) return "dark"
        if (html.classList.contains("light")) return "light"
        if (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) return "dark"
        return "light"  // default
      }

      let themeSelectionElement = document.createElement("a")
      themeSelectionElement.className = "nav__link"
      themeSelectionElement.id="theme-selector"
      themeSelectionElement.onclick=toggleTheme
      themeSelectionElement.innerText = (currentColorScheme() == "dark") ? "🌞" : "🌑"
      siteNav.appendChild(themeSelectionElement)

      function toggleTheme() {
        if (currentColorScheme() == "light") {
          html.classList.remove("light")
          html.classList.add("dark")
          themeSelectionElement.innerText = "🌞"
          sessionStorage.setItem("theme", "dark")
        } else {
          html.classList.remove("dark")
          html.classList.add("light")
          themeSelectionElement.innerText = "🌑"
          sessionStorage.setItem("theme", "light")
        }
      }
    </script>
  </body>
</html>
