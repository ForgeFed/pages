<!doctype html>
<html class="default">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ForgeFed</title>
    <link rel="stylesheet" type="text/css" href="/theme.css" />
    <link rel="icon" href="/img/logo.svg" />
    <script>
      // check whether the default colour scheme has been overridden
      // must be done as early as possible to avoid FOUC
      let theme = sessionStorage.getItem("theme")
      if (theme) document.documentElement.classList.add(theme)
    </script>
    <link rel="alternate" type="application/atom+xml" title="ForgeFed Blog" href="/atom.xml">
  </head>
  <body class="body">
    <header class="header">
      <h1 class="header-title">
        <a class="header-title__link" href="/">
            <img src="/img/logo-with-name.svg" alt="ForgeFed" title="ForgeFed" />
        </a>
      </h1>
      <nav class="nav" id="site-nav">
        <a class="nav__link"
           href="/spec">📓 specification</a>
        <a class="nav__link"
           href="/blog">📜 blog</a>
        <a class="nav__link"
           href="https://codeberg.org/ForgeFed/ForgeFed/issues">🐞 issues</a>
        <a class="nav__link"
           href="https://socialhub.activitypub.rocks/c/software/forgefed/60">🗒️ forum</a>
        <a class="nav__link"
           href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">💬 chat</a>
      </nav>
    </header>
    <main class="main">
      
  <h1 class="main-title">Vervis Actor Refactoring</h1>
  <p>
    2023-05-25 by Pere Lev
  </p>
  <p>For the last 2 months, I've been working on a major refactoring of the Vervis
codebase. I'm switching the architecture and code structure from a traditional
RESTful web API model into a networked <em>actor model</em>. I'm very excited about
it, because it's about much much more than just refactoring code: It's about
creating <strong>a new kind of application</strong>. And it's about the creating the <strong>tools
and foundations that enable federated actor-based applications</strong>.</p>
<h2 id="vervis">Vervis</h2>
<p>Vervis is my implementation of a federated forge server. It started in 2016,
before the ActivityPub era, when the social Fediverse was based on OStatus.
When ActivityPub gained momentum with Mastodon's adoption of it, and ForgeFed
formed, I implemented ActivityPub-based federation in Vervis, and kept
implementing and experimenting with features, in parallel to developing them in
the ForgeFed specification.</p>
<p>Vervis is a reference implementation of ForgeFed, and is meant to be used for
testing other ForgeFed implementations (such as Forgejo).</p>
<p>While Vervis is on the proof-of-concept level, and not a &quot;production-quality&quot;
forge like Gitea, Forgejo, GitLab, etc., <em>I do intend to bring it to that level
eventually</em>. At least to the level that it can serve as the basis for a network
of federated forges where people can get a taste of what it looks and feels
like, to develop software collaborative on a decentralized by-the-people
network.</p>
<ul>
<li>Main repo: <a href="https://vervis.peers.community/repos/rjQ3E">https://vervis.peers.community/repos/rjQ3E</a></li>
<li>Mirror on Codeberg: <a href="https://codeberg.org/ForgeFed/Vervis">https://codeberg.org/ForgeFed/Vervis</a></li>
<li><a href="https://fedi.foundation/2022/09/vervis-federated-merge-requests">Federated PR demo from Sep 2022</a></li>
</ul>
<p>Right now we have the federated forge network project moving in two parallel
paths:</p>
<ol>
<li>The ForgeFed implementation in <a href="https://forgejo.org">Forgejo</a>, along with UI
changes to support display of federated objects and actors. Once the
implementation reaches a certain level of readiness,
<a href="https://codeberg.org">Codeberg</a> will probably deploy it</li>
<li>The Vervis path I'll describe below</li>
</ol>
<p>While Forgejo is a stable widely deployed forge, and Vervis an experimental
buggy PoC-level piece of software, Vervis also enjoys being <strong>federated at the
core</strong>.</p>
<p>The Vervis path involves:</p>
<ul>
<li>Removing all UI code from Vervis, making it a backend API-server only</li>
<li>Debugging and stabilizing it, having a clear API for federation and for
client apps with a reliable implementation</li>
<li>Developing the <a href="https://codeberg.org/Anvil/Anvil">Anvil</a> frontend app,
reaching a usable demo release</li>
</ul>
<h2 id="the-old-architecture">The old architecture</h2>
<p>Until the refactoring, Vervis was written like a traditional web app server,
with its API being split into a server-to-server federation API (S2S), and a
command API for clients (C2S). All data (except Git and Darcs repos) lived in a
single big PostgreSQL database, including all the data of all the actors:
People, repositories, issue trackers, etc.</p>
<p>This architecture may work for centralized apps and services, but for Vervis,
I was gradually feeling more and more the impact of this structure. The primary
challenges were:</p>
<ol>
<li>Activity handler logic was duplicated: Response to activities of local
actors was happening in the C2S API handlers, while response to activities
of remote actors was happening in the S2S API handlers.</li>
<li>Since actors are just rows in the DB, an actor that receives an activity
needs to take care to initiate the whole chain of events needed in response
to the activity, often involving cooperation between multiple actors, making
sure to insert activities into inboxes and deliver and forward activities to
remote actors and collections. This resulted in activity handlers being
complicated, fragile and bug-prone.</li>
</ol>
<p>During my work in 2022 it became clear that I need a new architecture: A system
of actors that send and receive messages, and where message handler code is
high-level can implement the logic in one place, without separate paths for
local and remote actors.</p>
<p>It also became clear to me, that ActivityPub actually isn't really suited for
such actor-model based programming. It's a vocabulary for social actors and
notifications, not a full actor-model programming system. Being frustrated with
the complexity of writing Vervis actors, I started looking for alternatives.
The next stage of the Fediverse, perhaps. I found 2 primary tools that seemed
relevant:</p>
<ul>
<li><a href="https://spritely.institute">Spritely</a></li>
<li><a href="https://capnproto.org">Cap'n Proto</a></li>
</ul>
<p>Spritely is is exactly the kind of tool I was looking for, but still at an
unstable early stage, and works only with Guile and Racket (while Vervis is
written in Haskell). Cap'n Proto is mature and used in &quot;production&quot; systems,
but its Haskell library (and I suspect the other implementations too) is much
more low-level and doesn't provide a batteries-included way to conveniently
write high-level distributed application code.</p>
<p>I decided to start an experiment: A Cap'n Proto based Fediverse demo, that
would demonstrate - to me and to others - what it might look like, if Mastodon
was implemented on top of Cap'n Proto or Spritely or similar, rather than
ActivityPub. It's nicknamed the <a href="https://codeberg.org/fr33domlover/Capybara">Capybara
experiment</a>. &quot;Capybara&quot; because
it's about capability-based programming.</p>
<p>While working with <a href="https://nlnet.nl">NLNet</a> on the funding plan (<a href="https://todo.towards.vision/share/lecNDaQoibybOInClIvtXhEIFjChkDpgahQaDlmi/auth?view=kanban">here's my
kanban board</a>,
I decided to give a chance to the ActivityPub-based ForgeFed, and to dedicate
my work in 2023 to stabilize ForgeFed as an ActivityPub-based forge federation
protocol that covers the basic features that common forges provide - primarily
code, issues and PRs - and prepare the tools around Vervis to use as a test
suite for other implementations and as a reusable tool that people can study
and built more with. With that in place, I would move on with the Capybara
experiment and create an actor-programming based &quot;ForgeFed 2&quot; (or even give it
a new name).</p>
<h2 id="objects-capabilities-and-actors">Objects, capabilities and actors</h2>
<p>The biggest task on my plan for 2023, which I chose as the first task to do
within my NLNet-funded plan, is a refactoring of the Vervis codebase, to be
based on the same kind of actor-programming RPC system that Spritely and Cap'n
Proto have.</p>
<p>It means the structure of the program isn't a set of handlers of HTTP REST web
routes, but instead a set of actor method handlers.</p>
<p>Actors are objects that exist and live in parallel, and asynchronously send
messages to each other. Each type of actor has its own set of methods, and
handlers that implements its reaction to its methods being called by other
actors. Within a method handler, the actor can do just 3 things:</p>
<ol>
<li>Modify/update its own state</li>
<li>Launch a finite number of new actors</li>
<li>Terminate</li>
</ol>
<p>So, the program enables a network of actors, running in parallel and exchanging
messages.</p>
<p>Instead of a big shared database, each actor has its own state, that it manages
privately.</p>
<p>This system greatly simplifies writing concurrent code and data queries.</p>
<p>In a networked actor system, these actors can run on different machines, and
communicate via a network. But this communication is completely transparent to
the programmer: Calling a method of a local actor, and calling a method of a
remote actors, look exactly the same. You just write the <em>logic</em> of your
program, in terms of actors (think of these as a kind of microservices)
exchanging methods (which are commands, requests and events), and the
networking part is done for you by the actor system. This makes it <em>very</em> easy
and convenient to effortlessly write decentralized, federated and distributed
network/web applications!</p>
<p>These objects-actors are also sometimes called capabilities. This refers to
capability-based programming and capability-based security: Instead of using
things like ACLs, RBAC, passwords, and have-it-all ambient authority (e.g.
programs having free access to the filesystem, network, hardware, etc.), your
access to resources is based on <em>what you have</em>: If you have a reference to an
actor that representes a certain resource, you can access the resource via the
actor's methods. And if you want to give someone access to a resource, then
instead of giving them a password or adding them to a ACL, you send them a
reference to a relevant actor, so they can call its methods.</p>
<p>One of the principles in such systems is &quot;designation is authorization&quot;.
There's no separatation between a reference to a resource (e.g. a link to an
online document) and the authority to access it (e.g. a secret password letting
you edit the document). An object/actor/capability is both the resource and the
authority to access it (possibly just to observe, possibly to also edit). The
resource reference and the authoity are the same thing.</p>
<h2 id="material-and-representational-capabilities">Material and representational capabilities</h2>
<p>So, I'm facing a challenge:</p>
<ul>
<li>On one hand, I want to use a networked-capability-actor system in Vervis</li>
<li>On the other hand, I'm trying to <strong>still use ActivityPub for the network
layer</strong>, and ActivityPub is quite limiting here:
<ul>
<li>No clear distinction netween commands and events</li>
<li>No schema files for easily creating small-scale actors, the focus is on
domain-level social actors (person, organization, repository, etc.)</li>
<li>No real methods with clear typed parameters, instead there are
mostly-arbitrary and freely-extensible JSON-LD objects called Activities</li>
<li>No real capabilities</li>
</ul>
</li>
</ul>
<p>I'd like to look deeper at the last item, &quot;no real capabilities&quot;.</p>
<p>The kind of actor system I described above is <em>behavior based</em>. Data,
databases, tables, formats, migrations, linked data etc. etc. aren't a
first-class member in such a system. Obviously, actors would store their state
on disk using some file format. Maybe in a SQL database, maybe in a Git repo,
maybe in some binary-encoded file. But the behavior of the system doesn't
depend on it, you don't access the data directly. The medium of interaction is
<em>actor methods calls</em>.</p>
<p>The &quot;pro&quot; you gain in this model: There's a precise and convenient interface
for defining actor behaviors. You define methods, their names and their
parameters, and the types of these parateres. Concurrency and scaling are
built-in, no fiddling with low-level data format details. Atomicity is also
built-in: Actor method calls and all the calls they cause can form a single
transaction, that either failes and rolls, or happens in complete success.</p>
<p>The &quot;con&quot; is that there's no built-in shared data layer. No protocol for
efficient access to specialized kinds of data. If you need these things, you
either build them on top, or use protocols external to the actor system.</p>
<p>The kind of capabilities is what I referred to as &quot;real&quot; capabilities. That's
basically literally and materially, designation is authorization. You literally
gain access to a resource by receiving a pointer to an actor (AKA object AKA
capability). That pointer is literally how you invoke the actor's methods.
Hence I (until I discover a better name) call these capabilities <strong>material</strong>
capabilities.</p>
<p>In contrast, ActivityPub (and <a href="https://atproto.com/">Bluesky</a> perhaps even
more) heavily leans towards a <em>data based</em> approach, rather than a behavior
based one. Instead of methods with precise typed named parameters and return
values, there are &quot;activities&quot;, which confusingly double as both commands and
events, and are extensible linked data documents, where even the standard
properties are generic ones like &quot;origin&quot;, &quot;context&quot;, &quot;instrument&quot;, &quot;object&quot;
etc. whose meaning changes based on the type of object or activity.</p>
<p>The &quot;pro&quot; you gain in this approach is that notifications, inboxes and outboxes
containing descriptions of all the activity of actors ar built-in. Linked data
available to stream, mix, match, process, query and display. And the data is
very extensible, easily extensible, both activities and other objects.</p>
<p>The &quot;con&quot; is that having precise clear APIs beyond trivial commands is
difficult, and complex sequences of &quot;method calls&quot; are very cumbersome to
implement. Instead of having the meaning of a command be clearly stated in it,
the command arrives as a more general event description, and the exact meaning
needs to be determined based on the parameters, types and context of previous
activities.</p>
<p>In particular, a consequence of this approach is that when using
capability-based security, you need to choose between:</p>
<ol>
<li>Modeling methods, parameters and return values <em>on top</em> of the JSON activity
based layer, thus using a single &quot;Invoke&quot; activity for all method calls,
instead of descriptive extensible linked data like &quot;Create&quot;, &quot;Follow&quot;,
&quot;Update&quot;, &quot;Invite&quot;, etc.</li>
<li>Using &quot;fake&quot; capabilities where the &quot;pointer&quot; allowing the method call is
merely attached to the activity description, rather than being the actual
way of calling the method, i.e. break the &quot;designation is authorization&quot;
principle by separating resource IDs from the authority to access them</li>
</ol>
<p>If the &quot;real&quot; capabilities from earlier are <em>material</em>, then these &quot;fake&quot;
capabilities I call <strong>representational</strong>. Because they <em>represent</em> capabilities
using a JSON-LD linked data vocabulary, but nothing is enforcing that these
capabilities are actually used for authorization. They're like a key or
password that's attached to a command, and the receiving actor is responsible
for using it in the intended way. Such capabilities are <em>imitating</em> material
capabilities.</p>
<p>When I started working on bringing capabilities to ForgeFed, this is a choice I
faced: Do we create a whole new paradigm of modeling commands on the Fediverse,
by describing method schemas using JSON-LD and using a single non-descriptive
&quot;Invoke&quot; activity to call them? Or do we maintain the common use of activity
descriptions doubling as both commands and events, but at the cost of
representational, &quot;fake&quot; capabilities, and non-trivial event sequences being
very cumbersome and inconvenient to implement?</p>
<p>It might seem surprising, that I went for the 2nd option. I did so because the
1st option, despite being quite appealing, would cause ForgeFed to diverge from
&quot;regular&quot; ActivityPub to a degree that would almost defeat the point of using
ActivityPub, connecting forges with the wider Fediverse. Forges would seem to
be using some peculiar unusual protocol, that other Fediverse software wouldn't
recognize.</p>
<h2 id="the-new-architecture">The new architecture</h2>
<p>In the last 2 months or so, I wrote an actor-system library in Haskell, for use
in Vervis (and hopefully elsewhere too). Below are links to some core pieces of
the code.</p>
<blockquote>
<p>Module <code>Control.Concurrent.Actor</code>
<a href="https://vervis.peers.community/repos/rjQ3E/source-by/main/src/Control/Concurrent/Actor.hs">on Vervis</a>,
<a href="https://codeberg.org/ForgeFed/Vervis/src/branch/main/src/Control/Concurrent/Actor.hs">on Codeberg mirror</a></p>
</blockquote>
<p>I hooked my ActivityPub code into it, to enable the networking between actors.</p>
<blockquote>
<p>Module <code>Web.Actor.Deliver</code>
<a href="https://vervis.peers.community/repos/rjQ3E/source-by/main/src/Web/Actor/Deliver.hs">on Vervis</a>,
<a href="https://codeberg.org/ForgeFed/Vervis/src/branch/main/src/Web/Actor/Deliver.hs">on Codeberg mirror</a></p>
</blockquote>
<p>I even started implementing per-actor storage to gradually replace the
PostgreSQL database.</p>
<blockquote>
<p>Module <code>Database.Persist.Box</code>
<a href="https://vervis.peers.community/repos/rjQ3E/source-by/main/src/Database/Persist/Box/Internal.hs">on Vervis</a>,
<a href="https://codeberg.org/ForgeFed/Vervis/src/branch/main/src/Database/Persist/Box/Internal.hs">on Codeberg mirror</a></p>
</blockquote>
<p>And I started porting the <code>Person</code> actor to this new system.</p>
<blockquote>
<p>Module <code>Vervis.Actor.Person</code>
<a href="https://vervis.peers.community/repos/rjQ3E/source-by/main/src/Vervis/Actor/Person.hs">on Vervis</a>,
<a href="https://codeberg.org/ForgeFed/Vervis/src/branch/main/src/Vervis/Actor/Person.hs">on Codeberg mirror</a></p>
</blockquote>
<p>If you look at that <code>Vervis.Actor.Person</code> module, you might notice that the
handlers for locally-sent events and the handlers for remotely-sent events are
separate. Isn't that exactly what I was trying to avoid, aiming to have a
<em>single</em> place to implement actor logic? Indeed, that's my intention. But with
ActivityPub as the basis for ForgeFed, it's difficult: Local method calls can
be precise requests, while remote calls are stuck using Activities as method
calls. And each activity is an extensible linked data object that needs to be
parsed, and its internal consistency verified. In addition, activity handlers
in the old architecture were using the shared PostgreSQL database to query the
context of previous activities. That's partially because activities aren't live
objects with live references to other related actors. So I may need a new way
to write safe validating method call handlers.</p>
<p>Ah, the ActivityPub activities double as commands and events, which makes it
impossible to determine the meaning of the activity without looking carefully
at the parameters and previous context, in a way that varies based on the types
of actors and type of activity.</p>
<p>So, to be honest, I'm not sure yet how or whether the local and remote handlers
can be converged into a single thing. Maybe it's not practical as long as I'm
using ActivityPub. Despite that, this architecture still greatly simplifies the
implementation of method handlers.</p>
<h2 id="what-s-next">What's next</h2>
<p>In the new architecture, the actor-system, there's still a lot to do:</p>
<ul>
<li>Port all actors to it</li>
<li>Port the C2S API handlers to it</li>
<li>Switch to full per-actor storage</li>
<li>Use thread supervisors for actor threads, instead of plain <code>forkIO</code></li>
<li>Switch to full capability-based programming of actors, instead of using the
<code>IO</code> monad that allows any actor to perform any side effect</li>
</ul>
<p>And the actor system can be improved way beyond ActivityPub:</p>
<ul>
<li>Implement transactions, where a single sequence of calls either happens to
completion, or gets rolled back (how does Spritely do that? Perhaps have each
actor return an STM action that updates its state, and upon completion run
those STM actions together in a single transaction?)</li>
<li>Implement CapTP, promise pipelining, etc. etc.</li>
<li>Implement network layers and OcapN</li>
<li>Get rid of <code>TheaterFor</code> by passing live <code>ActorRef</code>s instead of textual IDs</li>
</ul>
<p>I'm actually considering to do the &quot;Capybara experiment&quot; using this actor
system, instead of Cap'n Proto or Spritely! My funded task plan <em>does</em> include
the experiment, but it's only a small part. Perhaps in 2024 when I'm done with
the current tasks, I'll shift my focus to the capability-based system and maybe
even a new phase of ForgeFed that will be based on it.</p>
<p>Anyway, landing back to reality, my next tasks in 2023 with ForgeFed and Vervis
are about:</p>
<ul>
<li>Finalizing the (representational) capability system in ForgeFed</li>
<li>Finishing the specification of projects and teams</li>
<li>Finishing the implementiion of all of that in Vervis</li>
<li>Turning Vervis into a test suite for other implementations</li>
</ul>
<h2 id="comments">Comments</h2>
<p>We have an account for ForgeFed on the Fediverse:
<a href="https://floss.social/@forgefed">https://floss.social/@forgefed</a></p>
<p>Right after publishing this post, I'll make a toot there to announce the post,
and you can comment there :)</p>


    </main>
    <footer class="footer">
      <p xmlns:dct="http://purl.org/dc/terms/">
        <a rel="license"
           href="http://creativecommons.org/publicdomain/zero/1.0/">
          <img src="https://licensebuttons.net/p/zero/1.0/88x31.png"
               style="border-style: none;"
               alt="CC0" />
        </a>
        <br />
        <a rel="dct:publisher" href="https://forgefed.org/">
          <span property="dct:title">The ForgeFed team</span>
        </a>
        has dedicated all copyright and related and neighboring
        rights to
        <span property="dct:title">ForgeFed</span> to the public domain
        worldwide.
      </p>
      <p>❤ Copying is an act of love. Please copy, reuse and share!</p>
      <p>
        Site generated with
        <a class="footer__link" href="https://www.getzola.org">Zola</a>
        and
        <a class="footer__link" href="https://tabatkins.github.io/bikeshed">Bikeshed</a>.
      </p>
      <p>
        <a href="https://liberapay.com/ForgeFed/donate">
          <img alt="Donate using Liberapay"
              src="https://liberapay.com/assets/widgets/donate.svg" />
        </a>
        <a href="https://opencollective.com/forgefed">
          <img alt="Donate using Open Collective"
              src="/img/open_collective.svg" />
        </a>
      </p>
    </footer>
    <script>
      let html = document.documentElement
      let siteNav = document.getElementById("site-nav")

      function currentColorScheme() {
        if (html.classList.contains("dark")) return "dark"
        if (html.classList.contains("light")) return "light"
        if (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) return "dark"
        return "light"  // default
      }

      let themeSelectionElement = document.createElement("a")
      themeSelectionElement.className = "nav__link"
      themeSelectionElement.id="theme-selector"
      themeSelectionElement.onclick=toggleTheme
      themeSelectionElement.innerText = (currentColorScheme() == "dark") ? "🌞" : "🌑"
      siteNav.appendChild(themeSelectionElement)

      function toggleTheme() {
        if (currentColorScheme() == "light") {
          html.classList.remove("light")
          html.classList.add("dark")
          themeSelectionElement.innerText = "🌞"
          sessionStorage.setItem("theme", "dark")
        } else {
          html.classList.remove("dark")
          html.classList.add("light")
          themeSelectionElement.innerText = "🌑"
          sessionStorage.setItem("theme", "light")
        }
      }
    </script>
  </body>
</html>
