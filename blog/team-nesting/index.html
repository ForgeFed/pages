<!doctype html>
<html class="default">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ForgeFed</title>
    <link rel="stylesheet" type="text/css" href="/theme.css" />
    <link rel="icon" href="/img/logo.svg" />
    <script>
      // check whether the default colour scheme has been overridden
      // must be done as early as possible to avoid FOUC
      let theme = sessionStorage.getItem("theme")
      if (theme) document.documentElement.classList.add(theme)
    </script>
    <link rel="alternate" type="application/atom+xml" title="ForgeFed Blog" href="/atom.xml">
  </head>
  <body class="body">
    <header class="header">
      <h1 class="header-title">
        <a class="header-title__link" href="/">
            <img src="/img/logo-with-name.svg" alt="ForgeFed" title="ForgeFed" />
        </a>
      </h1>
      <nav class="nav" id="site-nav">
        <a class="nav__link"
           href="/spec">📓 specification</a>
        <a class="nav__link"
           href="/blog">📜 blog</a>
        <a class="nav__link"
           href="https://codeberg.org/ForgeFed/ForgeFed/issues">🐞 issues</a>
        <a class="nav__link"
           href="https://socialhub.activitypub.rocks/c/software/forgefed/60">🗒️ forum</a>
        <a class="nav__link"
           href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">💬 chat</a>
      </nav>
    </header>
    <main class="main">
      
  <h1 class="main-title">Team Nesting</h1>
  <p>
    2024-05-17 by Pere Lev
  </p>
  <p>The previous post was about project nesting. Most of what I said there probably
applies to team nesting as well, and the implementation of team nesting reuses
the same mechanism. The <em>next</em> step, connecting teams and projects, is going to
be big. But today I'll talk about this in-between step: Team nesting.</p>
<p>As always, my <a href="https://todo.towards.vision/share/lecNDaQoibybOInClIvtXhEIFjChkDpgahQaDlmi/auth?view=kanban">task board</a> is available!</p>
<h2 id="organizational-structure">Organizational Structure</h2>
<p>Every organization has systems for how collaboration and communication works.
Sometimes these are conscious systems, explicitly defined. And sometimes
they're implicit. When these systems aren't defined and discussed, what
probably often happens is that our old habits and cultural conditioning take
the stage. So, whether we're aware of it or not, the systems are there.</p>
<p>Some of these systems are:</p>
<ul>
<li>Resource flow: How do resources (money, equipment, support, working hands,
etc. etc.) flow within the organization, and into it and out of it? Which
mechanisms control that?</li>
<li>Decision making: Who makes decisions on what, and using which methods?</li>
<li>Feedback flow: When, where and how is feedback given between people and teams
in the organization, and how is it integrated? Which mechanisms support
people's growth and an evolution of the organization's vision, purpose and
strategies?</li>
<li>Conflict engagement: How are conflicts dealt with?</li>
<li>Information flow: How does information flow within the organization, who
knows what and why and how?</li>
<li>Care and support: When people and teams have personal or relational needs
that affect their capacity with respect to the organization's purpose, how
are those needs attended, how are difficulties supported?</li>
</ul>
<p>I was actually going to do a remote workshop on LibrePlanet about this :-) I
guess they had more relevant candidates for the remote slots. Anyway, I'm
inviting you to examine your software projects, and organizations you're in,
through this lens of organizational systems! How do these things work in your
team or project?</p>
<p>I have a dream, a fantasy: A sofware forge that provides effective tools and
guidance for managing all these aspects, not just the plain usual
code-issues-PRs structure. For example, enhance the typical stream-of-comments
issue tracker to include a powerful decision-making tracker system. If you're
up for the challenge of designing and implementing such things, I'm up for
offering my guidance and consulting :-)</p>
<p>For now, the focus of ForgeFed is simply modeling the basic tools that
developers usually expect from a forge, and that the popular forges offer.
Therefore, &quot;organizational structure&quot; in ForgeFed is primarily within the
resource flow department, specifically <strong>access control</strong>: The system that
determines who has which access to which resources.</p>
<p>So, if your organization has 5 teams, all of them working on the same piece of
software and all having the same full access to the code, there might be no
need, in the ForgeFed sense, to create 5 <em>Team</em>s. Because the primary
contribution of creating teams, I believe much like in the common centralized
forges, is to define access control.</p>
<h2 id="team-nesting-for-access-control">Team Nesting for Access Control</h2>
<p>So how do we define access control using ForgeFed's team nesting? A previous
blog post already discussed teams, so I'm focusing here on the new feature: The
ability to <em>nest</em> teams, i.e. teams having subteams / child teams. Here's an
example of what it might look like:</p>
<p><img src="https://forgefed.org/blog/team-nesting/team-nesting.svg" alt="Example of organizational structure" /></p>
<p>So, <em>Employees</em> is a team that's going to have access to the common resources
that everyone in the organization uses. Then there's some subteams, which:</p>
<ol>
<li>Have access to their own resources, which aren't avilable for the rest of
the organization (e.g. perhaps the backend team has its own code
repostories)</li>
<li>Inherit the access that their parent teams have, e.g. people in the
<em>Backend</em> team automatically gain access to whatever the <em>Employees</em> team
has access to!</li>
<li>Further pass both of these to their own subteams</li>
</ol>
<p>Creating teams, and forming parent-child relationships, are separate actions in
ForgeFed. In a federated network, there's no hierarchy with someone at the top
who controls everything. So, cooperation between actors is based on <strong>mutual
consent</strong>. From the ForgeFed perspective, <em>Employees</em> and <em>Backend Team</em> are
just two independent self-contained <em>Team</em> actors, that have decided to form a
parent-child link between them. Neither of them can force the other to
cooperate: It works only as long as mutual consent exists.</p>
<p>However, representing hierarchical team structures with ForgeFed is entirely
possible. Here are some ideas:</p>
<ul>
<li>Create a <em>Managers</em> team that is a child of all other teams</li>
<li>Create an <em>Everything</em> project that is a parent of all projects in the
organization, and give managers access to this project</li>
</ul>
<p>The mechanism for forming and removing team parent-child links is similar to
the mechanism for projects:</p>
<ul>
<li>A human approves the link from one side</li>
<li>A human approves the link from the other side</li>
<li>Both teams approve the link AKA handshake</li>
<li>Child team tells the parent &quot;Okay, you can extend access tokens to me&quot;</li>
<li>Parent team, when it receives access tokens (AKA <em>Grant</em> activities), sends
extensions to the child</li>
</ul>
<h2 id="implementation">Implementation</h2>
<p>Much of this was already implemented while working on project nesting. So what
remained is to adapt the code from projects, and reuse the common pieces:</p>
<ul>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/533fc60fe1c867592b8d8dc0911477ccad7c5ae1">UI: Group: Implement parent/child process button POST handlers</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/74af2ea2232e101d70656cc11392ec1f6c06253d">UI: Group: Display parent and child invites &amp; approve-remove buttons</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/bef8a77d8446932b7cfc9659405bdee8bbd841d5">UI: Browse: List team parents</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/3162a6ac28b3287b3d65bccac00617f95438f000">S2S: Group: Implement Add handler based on Project</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/ee30cb9f70dddb81e8625480cc1376cce22cc94f">S2S: Project: Accept: Separate child and parent modes' code</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/d72e06727c912470ed7402a3ecd3be8d889ba0f5">S2S: Group: Accept: Port child-parent modes from Project</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/5e0a2e1088b2f24049fd63d4b8e2ff5a6f4f1baa">S2S: Group: Grant: Port parent-child modes from Project</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/1e69a6e952055b0b044ba2a9b138eb97e4d5ca3a">S2S: Group: Remove: Port parent-child modes from Project</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/6cb1c11141e6bb1ce5a6fee9c53ab843c8fb6c91">S2S: Group: Port Revoke handler from Project</a></li>
</ul>
<h2 id="funding">Funding</h2>
<p>I really want to thank NLnet for funding this work, and much more to come! My
grant has been extended, which allows me to continue :-)</p>
<h2 id="next-steps">Next Steps</h2>
<p>I'm so excited about the next piece! The extended and updated grant from NLnet
is allowing me to work on project-team links, and more generally
<strong>team-resource</strong> links. In other words, much like <em>Person</em>s can be
collaborators on resources, <em>Team</em>s will be able to be collaborators too. And
that task, which is already in progress, will complete the OCAP system vision
that's been forming for the past few years!</p>
<p>Some of the next challenges I see ahead:</p>
<ul>
<li>Fixing and completing all the basic features: Registration API, commenting,
PR merging, git-push by remote collaborators, resource and account deletion,
editable settings for all resources, probably a few more bits</li>
<li>Turning Vervis into a beyond-PoC stable usable forge, in particular the actor
logic, since the UI is going to be replaced</li>
<li>Figuring out if and how existing forges can implement federation, e.g.
Forgejo and GitLab</li>
<li>Implementing a fully functional usable frontend application (this is in
progress!)</li>
<li>Figuring out how to deploy a federated forge network, in particular the
human/social aspect of it, e.g. how to avoid the formation of few huge
company-owned instances where everyone is lured to flock, and how to form a
(probably international) team that will maintain and further develop the
software that powers the network <em>(I'm imagining all the eyes looking at me)</em></li>
</ul>
<h2 id="see-it-in-action">See It in Action</h2>
<p>I recorded a little demo of all this! <a href="https://tube.towards.vision/w/rPriJtLe216Y22sZtnaRVT">Watch it on my PeerTube
instance</a>.</p>
<p>If you want to play with things yourself, you can create account(s) on the demo
instances - <a href="https://fig.fr33domlover.site">fig</a>, <a href="https://grape.fr33domlover.site">grape</a>, <a href="https://walnut.fr33domlover.site">walnut</a> - and try the things I've mentioned
and done in the video.</p>
<p>If you encounter any bugs, let me know! Or <a href="https://codeberg.org/ForgeFed/Vervis/issues">open an
issue</a></p>
<h2 id="comments">Comments</h2>
<p>Come chat with us on
<a href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">Matrix</a>!</p>
<p>And we have an account for ForgeFed on the Fediverse:
<a href="https://floss.social/@forgefed">https://floss.social/@forgefed</a></p>
<p>Right after publishing this post, I'll make a toot there to announce the post,
and you can comment there :)</p>


    </main>
    <footer class="footer">
      <p xmlns:dct="http://purl.org/dc/terms/">
        <a rel="license"
           href="http://creativecommons.org/publicdomain/zero/1.0/">
          <img src="https://licensebuttons.net/p/zero/1.0/88x31.png"
               style="border-style: none;"
               alt="CC0" />
        </a>
        <br />
        <a rel="dct:publisher" href="https://forgefed.org/">
          <span property="dct:title">The ForgeFed team</span>
        </a>
        has dedicated all copyright and related and neighboring
        rights to
        <span property="dct:title">ForgeFed</span> to the public domain
        worldwide.
      </p>
      <p>❤ Copying is an act of love. Please copy, reuse and share!</p>
      <p>
        Site generated with
        <a class="footer__link" href="https://www.getzola.org">Zola</a>
        and
        <a class="footer__link" href="https://tabatkins.github.io/bikeshed">Bikeshed</a>.
      </p>
      <p>
        <a href="https://liberapay.com/ForgeFed/donate">
          <img alt="Donate using Liberapay"
              src="https://liberapay.com/assets/widgets/donate.svg" />
        </a>
        <a href="https://opencollective.com/forgefed">
          <img alt="Donate using Open Collective"
              src="/img/open_collective.svg" />
        </a>
      </p>
    </footer>
    <script>
      let html = document.documentElement
      let siteNav = document.getElementById("site-nav")

      function currentColorScheme() {
        if (html.classList.contains("dark")) return "dark"
        if (html.classList.contains("light")) return "light"
        if (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) return "dark"
        return "light"  // default
      }

      let themeSelectionElement = document.createElement("a")
      themeSelectionElement.className = "nav__link"
      themeSelectionElement.id="theme-selector"
      themeSelectionElement.onclick=toggleTheme
      themeSelectionElement.innerText = (currentColorScheme() == "dark") ? "🌞" : "🌑"
      siteNav.appendChild(themeSelectionElement)

      function toggleTheme() {
        if (currentColorScheme() == "light") {
          html.classList.remove("light")
          html.classList.add("dark")
          themeSelectionElement.innerText = "🌞"
          sessionStorage.setItem("theme", "dark")
        } else {
          html.classList.remove("dark")
          html.classList.add("light")
          themeSelectionElement.innerText = "🌑"
          sessionStorage.setItem("theme", "light")
        }
      }
    </script>
  </body>
</html>
