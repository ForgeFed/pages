<!doctype html>
<html class="default">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ForgeFed</title>
    <link rel="stylesheet" type="text/css" href="/theme.css" />
    <link rel="icon" href="/img/logo.svg" />
    <script>
      // check whether the default colour scheme has been overridden
      // must be done as early as possible to avoid FOUC
      let theme = sessionStorage.getItem("theme")
      if (theme) document.documentElement.classList.add(theme)
    </script>
    <link rel="alternate" type="application/atom+xml" title="ForgeFed Blog" href="/atom.xml">
  </head>
  <body class="body">
    <header class="header">
      <h1 class="header-title">
        <a class="header-title__link" href="/">
            <img src="/img/logo-with-name.svg" alt="ForgeFed" title="ForgeFed" />
        </a>
      </h1>
      <nav class="nav" id="site-nav">
        <a class="nav__link"
           href="/spec">📓 specification</a>
        <a class="nav__link"
           href="/blog">📜 blog</a>
        <a class="nav__link"
           href="https://codeberg.org/ForgeFed/ForgeFed/issues">🐞 issues</a>
        <a class="nav__link"
           href="https://socialhub.activitypub.rocks/c/software/forgefed/60">🗒️ forum</a>
        <a class="nav__link"
           href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">💬 chat</a>
      </nav>
    </header>
    <main class="main">
      
  <h1 class="main-title">Networked Actors</h1>
  <p>
    2025-01-28 by Pere Lev
  </p>
  <h2 id="new-year-new-work">New Year, New Work</h2>
<p>So, what's new for ForgeFed in 2025?</p>
<ul>
<li>The previous NLnet grant is about to close (we had an extension but I'm happy
to say we've used all the funding we received)</li>
<li>We're about to sign the new grant, which is going to have a slightly
different focus than before:
<ul>
<li>User research, gather the needs of forge federation developers</li>
<li>Figure out a path to get some federated collaboration in the real world,
e.g. by getting Forgejo instances to federate issues (but more likely:
Implement a (possibly simplified version of) ForgeFed OCAPs first)</li>
<li>Organize the spec and add human friendly documentation and API specs</li>
<li>Anvil frontend work</li>
<li>And yes, some Vervis development will continue</li>
<li>And yes, some Playwright actor system research will continue as well</li>
</ul>
</li>
</ul>
<p>This post is about the Playwright actor programming system, which now features
networking!</p>
<p>As always, my <a href="https://todo.towards.vision/share/lecNDaQoibybOInClIvtXhEIFjChkDpgahQaDlmi/auth?view=kanban">task board</a> is available.</p>
<h2 id="the-previous-step-relics">The Previous Step - Relics</h2>
<p>I've been giving nicknames to the layers of the actor system. In the previous
tasks I created 2 primary layers:</p>
<ul>
<li>Fly: General-purpose actor threads that receive messages</li>
<li>Relic: A persistence layer on top of Fly</li>
</ul>
<p>So, what we've had is a transparently disk-backed actor system! Since each
actor has its own thread, this system is actually meant for <em>Vats</em> rather than
individual actors. The actual actor part will come on top.</p>
<p>The next step on my plan was to add networking.</p>
<h2 id="the-new-layer-goose">The New Layer - Goose</h2>
<p><em>Goose</em> actors are a layer on top of <em>Relic</em>, which adds transparent networking
support. It currently uses plain TCP, without authentication. Each node on the
network has a host+port pair, and a TCP server that receives call requests and
inserts them into the local actor queues.</p>
<p>Goose actors use 2 types of Relics:</p>
<ol>
<li>Each Goose type <em>g</em> is implemented using a Relic <em>GooseWrap g</em></li>
<li>There's a specific new Relic type called <em>Remote</em>, used for calling methods
of remote actors</li>
</ol>
<p>Actor networking introduces 2 new pieces of the system:</p>
<ol>
<li>Needing to maintain a map of local actors, so that the TCP server can look
them up and insert calls to their queues</li>
<li>Tracking the <em>Remote</em> actors as well, so that actor mentions that appear in
method arguments can be converted into live references</li>
</ol>
<p>For this I added a <code>RelicMap</code> feature to the <code>Relic</code> module, for tracking local
actors. And in <code>Goose</code> there's a <code>RemoteTreasureMap</code> that tracks <code>Remote</code>
actors.</p>
<p>It's also possible for Goose actors to convert references between the data form
and the live actor form, which allows to copy and paste actors references and
connect actors on different machines!</p>
<p>There's no actual network protocol yet - I hope to use OCapN for that. At this
point, simple Haskell types are being used, along with their textual
serialization - one for method invocation and the other for reporting the
result. That's it.</p>
<p>But all of this may seem like hand waving. So I implemented a chat demo, to
show you the system in action :)</p>
<p>Here's the actual work:</p>
<ul>
<li>The Goose layer is in the
<a href="https://codeberg.org/Playwright/playwright/src/branch/main/src/Control/Concurrent/Goose.hs">Control.Concurrent.Goose</a>
module</li>
<li>The <code>Relic</code> module got some updates too, primarily this commit:
<a href="https://codeberg.org/Playwright/playwright/commit/6dc70895da22cef3d38f5a3abe8cf02d32a764cf">Relic, Goose: Introduce calling into the actor map</a></li>
<li><a href="https://codeberg.org/Playwright/playwright/src/branch/main/demos/goose-chat.hs">goose-chat demo</a></li>
</ul>
<p>In the code and in the demo, there are mentions of &quot;Sturdy&quot; - a <em>SturdyRef</em> is
a persistent identifier referring to a live actor. In the future these
identifiers will be serialized as URIs, but for now the default Haskell type
display is being used.</p>
<h2 id="see-it-in-action">See It in Action</h2>
<p>To run the chat demo,</p>
<ol>
<li>Clone <a href="https://codeberg.org/Playwright/playwright">Playwright</a> into 2 separate directories, and run <code>stack build</code> in
each directory (don't worry, the builds will share all the dependency
libraries)</li>
<li>In each directory execute: <code>stack run goose-chat</code></li>
<li>Use localhost as the host for both, and pick some 2 available ports, e.g.
6111 and 6222</li>
<li>The program will ask whether to update the peer's address - answer with <em>Y</em>
and paste the &quot;GooseSturdy&quot; line from each program into the other</li>
<li>Now you can type chat lines into each program, and they will appear on the
other! We've established a chat channel.</li>
</ol>
<p>The &quot;GooseSturdy&quot; line is each peer's <em>Chat</em> actor identifier, and it looks
like this:</p>
<p><code>GooseSturdy {gsAddr = GooseAddr {gaHost = &quot;localhost&quot;, gaPort = 6111}, gsRelic = 2}</code></p>
<p>A URI form could probably look like this:</p>
<p><code>goose://localhost:6111/2</code></p>
<p>Which means &quot;actor ID 2 on server localhost:6111&quot;.</p>
<p>As you might notice, there's no mention of the actor type. I could add it, but
IIRC OCapN doesn't use it, and Spritely Goblins being written in Scheme leans
towards self-describing representations rather than relying on type schemas
(e.g. like Cap'n'Proto). So although on the Haskell level the type schemas are
present, the network serialization will try to mimic what Spritely does.</p>
<p>If you try the demo, let me know how it went!</p>
<h2 id="funding">Funding</h2>
<p>I really want to thank NLnet for funding this work! The extended grant is
allowing me to continue backend work, and allowing André to work on the
<a href="https://codeberg.org/Anvil/Anvil">Anvil</a> frontend.</p>
<p>And I'm excited to see what happens during 2025! Implementing federation isn't
a trivial task, but I hope we can get more of it into actual usable live
forges.</p>
<h2 id="comments">Comments</h2>
<p>Come chat with us on
<a href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">Matrix</a>!</p>
<p>And we have an account for ForgeFed on the Fediverse:
<a href="https://floss.social/@forgefed">https://floss.social/@forgefed</a></p>
<p>Right after publishing this post, I'll make a toot there to announce the post,
and you can comment there :)</p>


    </main>
    <footer class="footer">
      <p xmlns:dct="http://purl.org/dc/terms/">
        <a rel="license"
           href="http://creativecommons.org/publicdomain/zero/1.0/">
          <img src="https://licensebuttons.net/p/zero/1.0/88x31.png"
               style="border-style: none;"
               alt="CC0" />
        </a>
        <br />
        <a rel="dct:publisher" href="https://forgefed.org/">
          <span property="dct:title">The ForgeFed team</span>
        </a>
        has dedicated all copyright and related and neighboring
        rights to
        <span property="dct:title">ForgeFed</span> to the public domain
        worldwide.
      </p>
      <p>❤ Copying is an act of love. Please copy, reuse and share!</p>
      <p>
        Site generated with
        <a class="footer__link" href="https://www.getzola.org">Zola</a>
        and
        <a class="footer__link" href="https://tabatkins.github.io/bikeshed">Bikeshed</a>.
      </p>
      <p>
        <a href="https://liberapay.com/ForgeFed/donate">
          <img alt="Donate using Liberapay"
              src="https://liberapay.com/assets/widgets/donate.svg" />
        </a>
        <a href="https://opencollective.com/forgefed">
          <img alt="Donate using Open Collective"
              src="/img/open_collective.svg" />
        </a>
      </p>
    </footer>
    <script>
      let html = document.documentElement
      let siteNav = document.getElementById("site-nav")

      function currentColorScheme() {
        if (html.classList.contains("dark")) return "dark"
        if (html.classList.contains("light")) return "light"
        if (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) return "dark"
        return "light"  // default
      }

      let themeSelectionElement = document.createElement("a")
      themeSelectionElement.className = "nav__link"
      themeSelectionElement.id="theme-selector"
      themeSelectionElement.onclick=toggleTheme
      themeSelectionElement.innerText = (currentColorScheme() == "dark") ? "🌞" : "🌑"
      siteNav.appendChild(themeSelectionElement)

      function toggleTheme() {
        if (currentColorScheme() == "light") {
          html.classList.remove("light")
          html.classList.add("dark")
          themeSelectionElement.innerText = "🌞"
          sessionStorage.setItem("theme", "dark")
        } else {
          html.classList.remove("dark")
          html.classList.add("light")
          themeSelectionElement.innerText = "🌑"
          sessionStorage.setItem("theme", "light")
        }
      }
    </script>
  </body>
</html>
