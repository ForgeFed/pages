<!doctype html>
<html class="default">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ForgeFed</title>
    <link rel="stylesheet" type="text/css" href="/theme.css" />
    <link rel="icon" href="/img/logo.svg" />
    <script>
      // check whether the default colour scheme has been overridden
      // must be done as early as possible to avoid FOUC
      let theme = sessionStorage.getItem("theme")
      if (theme) document.documentElement.classList.add(theme)
    </script>
    <link rel="alternate" type="application/atom+xml" title="ForgeFed Blog" href="/atom.xml">
  </head>
  <body class="body">
    <header class="header">
      <h1 class="header-title">
        <a class="header-title__link" href="/">
            <img src="/img/logo-with-name.svg" alt="ForgeFed" title="ForgeFed" />
        </a>
      </h1>
      <nav class="nav" id="site-nav">
        <a class="nav__link"
           href="/spec">📓 specification</a>
        <a class="nav__link"
           href="/blog">📜 blog</a>
        <a class="nav__link"
           href="https://codeberg.org/ForgeFed/ForgeFed/issues">🐞 issues</a>
        <a class="nav__link"
           href="https://socialhub.activitypub.rocks/c/software/forgefed/60">🗒️ forum</a>
        <a class="nav__link"
           href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">💬 chat</a>
      </nav>
    </header>
    <main class="main">
      
  <h1 class="main-title">Hello Actor Programming</h1>
  <p>
    2024-10-13 by Pere Lev
  </p>
  <h2 id="stressful-times">Stressful Times</h2>
<p>Wow, two months passed since my last post. Two exhausting, but also exciting,
months of working on something entirely new and different: An actor programming
library. It took much more time than I hoped, and of course it's just the
beginning, but I'm so relieved now that I have some results to show.</p>
<p>I'll be honest with you: I depend financially on the funding from NLnet,
funding for which I'm deeply grateful. But it also sometimes means a lot of
stress, when a task becomes complicated and long, which delays the payment. I
can already imagine the advice coming my way: Create a variety of income
sources, it's more sustainable. And you're right! And I'm working on it. But in
the field of software, just FYI, I never actually worked in a company. It's
deeply special and important and dear to me, that my work's purpose continues
to be the loyal service of humankind and perhaps even the whole ecosystem that
is our planet, and I therefore work in a post-capitalism mode, releasing all my
work as Free Software, and there's no investors or expectations of profit. I
just need enough money for my house and food etc. etc., that's it.</p>
<p>But being a student has made my expenses much much higher and my schedule much
more busy. That's probably what has made these months stressful.</p>
<p>Thank you for being witnesses of my journey! Let's dive into the content.</p>
<p>As always, my <a href="https://todo.towards.vision/share/lecNDaQoibybOInClIvtXhEIFjChkDpgahQaDlmi/auth?view=kanban">task board</a> is available.</p>
<h2 id="activitypub-and-ocaps">ActivityPub and OCAPs</h2>
<p>ForgeFed is built on top of the federation protocol called ActivityPub. And
ActivityPub is designed around the publishing of personal user content: You
publish posts, images, videos, etc. and people can comment on them, and comment
on other people's comments. And over the years, the Fediverse is getting new
applications that implement services that match that pattern:</p>
<ul>
<li>Funkwhale, for publishing audio</li>
<li>Peertube, for publishing videos</li>
<li>PixelFed, for publishing images</li>
<li>Mastodon, for microblogging</li>
<li>Lemmy, for link aggregation</li>
<li>And much more...</li>
</ul>
<p>The collaboration patterns for these applications <em>can</em> be complicated, but
they're mostly simple:</p>
<ul>
<li>Anyone can publish content</li>
<li>Anyone can comment on anyone else's content</li>
<li>There are features to handle spam protection, user blocking, etc.</li>
</ul>
<p>I know, I know, implementing all of that isn't trivial. My point is that the
main scenario is the publishing of personal content: You make a video, you
upload it, done.</p>
<p>ForgeFed is unusual compared to this Fediverse landscape: The focus of forges
is on <em>collaboration</em> on shared resources. Of course you can just publish your
own personal Git repos and work alone, but the power of issues, PRs, etc.
shines when people can work together. So the collaboration patterns here are
more complex. Unlike a video, that you just upload once, a Git repo is an
editable resource that ongoingly receives changes.</p>
<p>Collaboration on editable resources on a decentralized network requires a
powerful authorization mechanism: A way to reliably determine who can do which
actions on which resources. But federated authorization is one of the things
ActivityPub doesn't define, and leaves it to us to figure out.</p>
<p>In the past 2 years or so, I've worked hard to build an Object Capability
system into ForgeFed - a vocabulary for granting and revoking permissions. It
goes a long way, and still, something just feels wrong: Trying to build rich
collaboration on top of a system very clearly suited for personal publishing.</p>
<p>That's why in the work plan for 2023-2024, I added items for exploring
something else: A forge based on <em>actor programming</em> instead of ActivityPub.</p>
<h2 id="actor-programming">Actor Programming</h2>
<p>While ActivityPub mostly a combination or email-like publishing and a
vocabulary for describing personal objects, <em>actor programming</em>, or
<em>capability-based programming</em>, focuses on behavior: You write your
application's source code in such a way that a piece of the software has access
only to what it needs, and the permissions are essentially combined with the
operations: If you have a reference to an operation, you're allowed to execute
it.</p>
<p><em>Networked</em> actor programming becomes possible by creating tokens that
represent operations, and passing these tokens to other actors possibly on
different machines over the network. However, instead of a custom JSON-based
vocabulary like ActivityPub, that represents domain objects (images, videos,
etc.), the data format is now behind the scenes, and encodes references to
<em>functions</em> (and actors and their methods), so that any piece of code you write
can become a capability you pass on the network and allow other actors to
execute it.</p>
<p>This is a vastly more powerful foundation that what the ActivityPub-based
ForgeFed has, and I've been playing with it for the past year, exploring,
reading a lot of material, asking questions.</p>
<p>I've mostly been observing how <a href="https://spritely.institute">Spritely</a> and <a href="https://capnproto.org">CapnProto</a> work, and aiming
to create something similar, even compatible.</p>
<p>Much of the work happened inside Vervis: I implemented an actor system right
inside it, and it's been powering Vervis for a while now. But at some point,
the HTTP-based API and the capability-based system had to depart and take
different directions.</p>
<h2 id="a-big-experiment">A Big Experiment</h2>
<p>Spritely, which I mentioned above, already has the foundations for networked
actor programming, and a possible route would have been to try implementing a
little forge on top. This is a great direction! Anyone feels like trying it? I
picked a different route: While Spritely focuses on the Scheme language, I
started work, inspired by the CanProto Haskell implementation, to implement an
actor-programming system in Haskell.</p>
<p>Haskell and its ecosystem do have facilities for actor programming, and the
powerful type system could also allow generating client code for other
programming languages, but most (if not all) actor related packages are meant
for concurrent and cloud computation, not for capability-based programming of
federated-network applications.</p>
<p>I knew diving into this would be a risk for me: It's an experiment, with very
little clarity about the chances for success, or how much time it would take.
So through the year I kept working on Vervis tasks, and waiting with the actor
programming experiment as I read and explore and gather more and more clarity
on how to implement it when the time comes.</p>
<p>The risk is financial: I'm not an academic researcher paid for my work hours.
If I start a huge task, I don't receive funds until it's done. So I need to
gather some savings, allowing me some buffer to work without needing immediate
income.</p>
<p>Despite the financially stressful times, I somehow did it. In August 2024, the
time came.</p>
<h2 id="basic-concepts">Basic Concepts</h2>
<p>So, I basically started implementing something in the spirit of Spritely
Goblins, in Haskell. Some parts, such as networking and petnames, are left for
my next tasks. I started with focusing just on the basics (which are quite
complex by themselves):</p>
<ul>
<li>Actors have methods</li>
<li>Methods can call other methods of other actors</li>
<li>Networking is transparent: Calling a method or a local and remote actors
looks the same, you don't care about it when you implement domain logic</li>
<li>Actors are grouped in groups called <em>Vats</em> - actors in the same Vat can call
each other's methods synchronously, like regular function calls</li>
<li>Actors in different vats can only do an <em>asynchronous</em> call - send out the
method parameters and the result of the method call will arrive later</li>
<li>The whole system can be serialized to disk, and restored to continue running</li>
<li>Actors can have state, which may include references to other actors</li>
<li>The sequence of method calls within each Vat is <em>atomic</em> - either it succeeds
and the Vat's state is updated, or any state changes are reverted</li>
</ul>
<p>I'm about to descibe the steps I've taken on the path to implement these
features. If you want to explore the bottom-line code, look at these:</p>
<ul>
<li><a href="https://codeberg.org/Playwright/playwright">https://codeberg.org/Playwright/playwright</a></li>
<li>In particular
<a href="https://codeberg.org/Playwright/playwright/src/branch/main/src/Control/Concurrent/Relic.hs">Control.Concurrent.Relic</a></li>
<li>And that module's
<a href="https://codeberg.org/Playwright/playwright/src/branch/main/test/Relic.hs">test</a></li>
</ul>
<p>Most tests do a little Fibonacci computation using actors, thus serving as a
simple usage example.</p>
<h2 id="utilities">Utilities</h2>
<p>I needed some helper modules to support the actor programming system, such as:</p>
<ul>
<li>Since actor state (as well as method parameters etc.) can contain live
references to actors (which are essentially threads), I needed a way to
switch between the live state and the serializable form which can be read and
written to disk. For that I wrote the
<a href="https://codeberg.org/Playwright/mold">mold</a> library, which allows to replace
live references with their serialized form and vice versa.</li>
<li>To generate unique actor IDs, I implemented a simple type-safe
<a href="https://codeberg.org/Playwright/playwright/src/branch/main/src/Data/NameGenerator">NameGenerator</a>,
which basically immitates the auto-increasing IDs that SQL DBs often use</li>
<li>Some actors need the ability to shut themselves down, while others run
forever / until garbage collected. The latter kind allows for static
references: If you hold a reference, you have a guarantee the actor is alive.
To encode that in the type system I wrote the
<a href="https://codeberg.org/Playwright/playwright/src/branch/main/src/Control/Concurrent/Lifespan.hs">Control.Concurrent.Lifespan</a>
module.</li>
<li>Finally, for the actor thread part, the
<a href="https://codeberg.org/Playwright/playwright/src/branch/main/src/Control/Concurrent/Exchange.hs">Control.Concurrent.Exchange</a>
module is full of building blocks for data exchange and method calls between
threads</li>
</ul>
<h2 id="high-level-api-based-on-vervis">High-Level API Based On Vervis</h2>
<p>My starting point was the Vervis
<a href="https://codeberg.org/ForgeFed/Vervis/src/branch/main/src/Control/Concurrent/Actor.hs">Control.Concurrent.Actor</a>
API, which does a lot of what I needed, but is limited by Vervis needing the
actors to be ForgeFed actors which communicate by publishing Activities. I
played with it created the
<a href="https://codeberg.org/Playwright/playwright/src/branch/main/src/Control/Concurrent/BeeZero.hs">BeeZero</a>
interface, which refers to actors as &quot;Bees&quot;. I knew I'd need to build the
actor programming system in layers, and tried to give a name to each layer :)</p>
<p>The BeeZero module supports both near actors (i.e. synchronous calls to actors
in the same Vat) and far actors (async calls to other Vats), and is only a
live system, without disk persistence.</p>
<ul>
<li>The module itself:
<a href="https://codeberg.org/Playwright/playwright/src/branch/main/src/Control/Concurrent/BeeZero.hs">src/Control/Concurrent/BeeZero.hs</a></li>
<li>Test:
<a href="https://codeberg.org/Playwright/playwright/src/branch/main/test/Bee.hs">test/Bee.hs</a></li>
</ul>
<h2 id="a-step-back-to-low-level">A Step Back To Low-Level</h2>
<p>Persistence requires to have unique tokens representing actor references, as
well as any other live object that needs to be serialized and loaded back,
restoring the system to exactly the same state it was before. Since <code>BeeZero</code>
just uses live references without attaching serializable IDs, I didn't build
persistence on top. No IDs also means Promises (future values waiting to be
returned from method calls) can't be persisted either, which led me to decide I
need to take a step back and start from a lower-level point.</p>
<p>Since state atomicity is on the Vat level, and since each Vat is a single
thread with a single event loop, I decided to implement an actor system layer
for Vats, and later add the near-actor feature on top, instead of the other way
around.</p>
<p>I call these <em>Fly</em> actors, in the <code>Control.Concurrent.Fly</code> module. Fly actors
run in their own threads, their methods have exactly one parameter, and no
return value. To return a value, the parameter needs to provide a way to send
back the result. A low-level foundation.</p>
<p>The idea is that Vats would be Fly actors, and the next layers would add near
actors inside Vats, essentially adapting the <code>BeeZero</code> code to work on top of
<code>Fly</code>.</p>
<ul>
<li>The module itself:
<a href="https://codeberg.org/Playwright/playwright/src/branch/main/src/Control/Concurrent/Fly.hs">src/Control/Concurrent/Fly.hs</a></li>
<li>Test:
<a href="https://codeberg.org/Playwright/playwright/src/branch/main/test/Fly.hs">test/Fly.hs</a></li>
</ul>
<h2 id="vat-persistence">Vat Persistence</h2>
<p>On top of Fly actors, I went on to implement a persistence layer. Each Fly
actor has a read-only piece and a read-write piece in its state, and both can
be serialized to and from disk. There's a foundation for adding multiple
serialization methods, but right now the implementation is coded to use a
simple file-based database format. A future improvement could be switching to
SQLite.</p>
<p>Migration is supported too, by allowing actors to provide a series of mappings
between the previous versions of their state.</p>
<p>I named these actors <em>Relic</em> actors. So, a Relic actor <em>implements</em> the Fly
interface, making sure to atomically store its state on each iteration of the
event loop.</p>
<p>Since serialization involves conversions between live objects and their
serializable IDs, loading the actor system happens in steps:</p>
<ol>
<li>Load all actor data including actor IDs</li>
<li>Perform migrations on actor state</li>
<li>For each actor ID, create the live objects in memory</li>
<li>In each actor's loaded state, now attach to each actor reference (that is
right now just an ID) the matching live object</li>
<li>Now that we have the live state for each actor, finally launch the actor
threads</li>
</ol>
<p>Since Relic actors just implement the Fly interface, they too have methods with
a single parameter and no return value. Of course, multiple parameters can
trivially be passed by passing a tuple as the single parameter. But to support
Promises and Promise Pipelining (calling methods using future values and
references), we'll need another layer on top of Relic.</p>
<ul>
<li>The module itself:
<a href="https://codeberg.org/Playwright/playwright/src/branch/main/src/Control/Concurrent/Relic.hs">src/Control/Concurrent/Relic.hs</a></li>
<li>Test:
<a href="https://codeberg.org/Playwright/playwright/src/branch/main/test/Relic.hs">test/Relic.hs</a></li>
</ul>
<p>The test demonstrates persistence: Each time it runs, the test computes the
next 5 Fibonacci numbers, and displays them. On the filesystem, <code>test/ng</code> file
is the state of the ID-generator, and the <code>test/root</code> directory keeps the state
of each actor type.</p>
<h2 id="what-s-next">What's Next</h2>
<p>That's all so far! So what we have in place is <em>Persistent single-actor Vats
(Relic actors)</em>. Near calls can be represented by generating token IDs in Relic
state, but a higher-level layer will properly implement near calls by adapting
the BeeZero code to Relic.</p>
<p>What to expect for this actor programming library project in 2025:</p>
<ul>
<li>Full documentation (the <code>Exchange</code> module I mentioned above has a detailed
tutorial, the other modules not yet because they're still rapidly changing)</li>
<li>Promises and promise pipelining</li>
<li>Support for event sources
<ul>
<li>Mouse and keyboard</li>
<li>Textual UI</li>
<li>Graphic/Web UI</li>
<li>Networking (TCP, HTTP)</li>
</ul>
</li>
<li>Petnames</li>
<li>An initial forge demo</li>
</ul>
<p>Of course 2024 isn't over yet! I'll continue with this work, as well as some
Vervis tasks and bug fixes.</p>
<h2 id="funding">Funding</h2>
<p>I really want to thank NLnet for funding this work! The extended grant is
allowing me to continue backend work, and allowing André to work on the
<a href="https://codeberg.org/Anvil/Anvil">Anvil</a> frontend.</p>
<h2 id="see-it-in-action">See It in Action</h2>
<p>So, the main artifact of this work is the <a href="https://codeberg.org/Playwright/playwright">Playwright</a> library. There's no UI
to show, so no video demo yet. But you can already start a Haskell project and
import the Playwright library and play with Relic actors. The test is a good
starting point.</p>
<p>If you encounter any bugs, let me know! Or <a href="https://codeberg.org/Playwright/playwright/issues">open an
issue</a>.</p>
<h2 id="comments">Comments</h2>
<p>Come chat with us on
<a href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">Matrix</a>!</p>
<p>And we have an account for ForgeFed on the Fediverse:
<a href="https://floss.social/@forgefed">https://floss.social/@forgefed</a></p>
<p>Right after publishing this post, I'll make a toot there to announce the post,
and you can comment there :)</p>


    </main>
    <footer class="footer">
      <p xmlns:dct="http://purl.org/dc/terms/">
        <a rel="license"
           href="http://creativecommons.org/publicdomain/zero/1.0/">
          <img src="https://licensebuttons.net/p/zero/1.0/88x31.png"
               style="border-style: none;"
               alt="CC0" />
        </a>
        <br />
        <a rel="dct:publisher" href="https://forgefed.org/">
          <span property="dct:title">The ForgeFed team</span>
        </a>
        has dedicated all copyright and related and neighboring
        rights to
        <span property="dct:title">ForgeFed</span> to the public domain
        worldwide.
      </p>
      <p>❤ Copying is an act of love. Please copy, reuse and share!</p>
      <p>
        Site generated with
        <a class="footer__link" href="https://www.getzola.org">Zola</a>
        and
        <a class="footer__link" href="https://tabatkins.github.io/bikeshed">Bikeshed</a>.
      </p>
      <p>
        <a href="https://liberapay.com/ForgeFed/donate">
          <img alt="Donate using Liberapay"
              src="https://liberapay.com/assets/widgets/donate.svg" />
        </a>
        <a href="https://opencollective.com/forgefed">
          <img alt="Donate using Open Collective"
              src="/img/open_collective.svg" />
        </a>
      </p>
    </footer>
    <script>
      let html = document.documentElement
      let siteNav = document.getElementById("site-nav")

      function currentColorScheme() {
        if (html.classList.contains("dark")) return "dark"
        if (html.classList.contains("light")) return "light"
        if (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) return "dark"
        return "light"  // default
      }

      let themeSelectionElement = document.createElement("a")
      themeSelectionElement.className = "nav__link"
      themeSelectionElement.id="theme-selector"
      themeSelectionElement.onclick=toggleTheme
      themeSelectionElement.innerText = (currentColorScheme() == "dark") ? "🌞" : "🌑"
      siteNav.appendChild(themeSelectionElement)

      function toggleTheme() {
        if (currentColorScheme() == "light") {
          html.classList.remove("light")
          html.classList.add("dark")
          themeSelectionElement.innerText = "🌞"
          sessionStorage.setItem("theme", "dark")
        } else {
          html.classList.remove("dark")
          html.classList.add("light")
          themeSelectionElement.innerText = "🌑"
          sessionStorage.setItem("theme", "light")
        }
      }
    </script>
  </body>
</html>
