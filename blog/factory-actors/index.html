<!doctype html>
<html class="default">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ForgeFed</title>
    <link rel="stylesheet" type="text/css" href="/theme.css" />
    <link rel="icon" href="/img/logo.svg" />
    <script>
      // check whether the default colour scheme has been overridden
      // must be done as early as possible to avoid FOUC
      let theme = sessionStorage.getItem("theme")
      if (theme) document.documentElement.classList.add(theme)
    </script>
    <link rel="alternate" type="application/atom+xml" title="ForgeFed Blog" href="/atom.xml">
  </head>
  <body class="body">
    <header class="header">
      <h1 class="header-title">
        <a class="header-title__link" href="/">
            <img src="/img/logo-with-name.svg" alt="ForgeFed" title="ForgeFed" />
        </a>
      </h1>
      <nav class="nav" id="site-nav">
        <a class="nav__link"
           href="/spec">📓 specification</a>
        <a class="nav__link"
           href="/blog">📜 blog</a>
        <a class="nav__link"
           href="https://codeberg.org/ForgeFed/ForgeFed/issues">🐞 issues</a>
        <a class="nav__link"
           href="https://socialhub.activitypub.rocks/c/software/forgefed/60">🗒️ forum</a>
        <a class="nav__link"
           href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">💬 chat</a>
      </nav>
    </header>
    <main class="main">
      
  <h1 class="main-title">Actor Factory</h1>
  <p>
    2024-08-11 by Pere Lev
  </p>
  <p>Until now, publishing a new resource, whether it's a comment or a project, has
been happening only locally, on the user's home server. It means that whoever
wanted to implement a ForgeFed-federated forge, would need to host the
implementations of all actor/resource/service types on one domain name, acting
as a big all-in-one forge.</p>
<p>This is no longer the case! And that's what this post is about. Actor creation
is obviously a part of the core of ForgeFed's API, so I decided it's time to
take care of this upgrade :)</p>
<p>As always, my <a href="https://todo.towards.vision/share/lecNDaQoibybOInClIvtXhEIFjChkDpgahQaDlmi/auth?view=kanban">task board</a> is available.</p>
<h2 id="federated-actions">Federated Actions</h2>
<p>Every request sent on the federated network is an interaction between two or
more actors. Some of these requests can occur between actors of different
servers, and some can't.</p>
<p>Examples of federation-supporting requests:</p>
<ul>
<li>Follow an (possibly remote) actor</li>
<li>Open an issue (possibly on a remote issue tracker)</li>
<li>Add a repository to a project (both of which might be on different servers)</li>
</ul>
<p>Examples of requests that don't support federation, or weren't until now:</p>
<ul>
<li>Create a user account (this happens between your client/browser and your home
instance of choice)</li>
<li>Editing admin settings of other instances</li>
<li><strong>Creating a project</strong></li>
</ul>
<p>Until now, creating a new actor (project, ticket tracker, etc.) was limited to
your home instance, i.e. the actor is launched on the same instance where your
account is.</p>
<p>On one hand, it makes sense: Why would other servers allow you to create
projects on them?</p>
<p>On the other hand:</p>
<ul>
<li>It's an artificial limitation, i.e. this action could easily support
federation</li>
<li>It blocks the possibility of implementing micro-services that live on
different domains, where each service specializes in a specific type(s) of
actor</li>
<li>It blocks the possibility of distributed teams to allow all their members to
create resources on the team's main instance</li>
</ul>
<h2 id="factory-actor">Factory Actor</h2>
<p>So, shall we make actor creation support federation? That's exactly what I did!</p>
<p>There's a new type of actor (which will find its way to the ForgeFed
specification very soon as well), called <strong>Factory</strong>. The only job of a Factory
is to create resource actors (everything except Person and Factory) on its home
instance.</p>
<p>With that piece in place, <em>Create</em> activities are no longer sent into &quot;thin
air&quot;, they're sent to a Factory. And the Factory handles creating the actor.
Person actors can no longer launch new projects by themselves.</p>
<p>Since a Factory it a resource actor, it can have its own collaborators and
teams that have access to it, and it implements the Object Capability system,
just like the other actor types. This means a Factory can have remote
collaborators, and remote teams, <strong>thus allowing remote users to create new
actors via the Factory</strong>.</p>
<p>But who factories the factory?</p>
<p>So, Factory is the only actor type that can be created in the &quot;old&quot; way, i.e.
without requiring a Factory. The client/browser sends a client-to-server (C2S)
<em>Create</em> activity to the Person's outbox, and the Person actor launches a new
Factory. However, we wouldn't want <em>any</em> user to be able to do this. So, in the
Vervis settings file, there's a list of users who are approved for creating new
Factory actors.</p>
<p>Summary of actor creation:</p>
<ul>
<li>Person: Created via the registration API (see file <code>API.md</code> in the Vervis
source repo for details)</li>
<li>Factory: Created via a C2S request to a Person actor, and limited to users
who are allowed to create factories (normally, the server admins)</li>
<li>Any other ForgeFed actor type: Created via a Factory</li>
</ul>
<h2 id="the-little-details">The Little Details</h2>
<p>I needed to figure out certain details before the implementation:</p>
<ul>
<li>How to set which actor types the Factory is able to create?</li>
<li>How to control who is allowed to create Factories, who is allowed to use
them, who is allowed to change their settings and delete them?</li>
<li>How to make a Factory automatically serve every new user on <em>another</em>
instance? e.g. so that people on the Person micro-service can create repos on
the Repository micro-service</li>
</ul>
<p>These details might get changes and updates, but right now here's how they
work:</p>
<ul>
<li>There's a new property <code>availableActorTypes</code> which specifies, for a given
Factory, which types it's allowed to create</li>
<li>That property is specified when Creating the Factory, and when editing it</li>
<li>Editing uses a new activity type <code>Patch</code> where <code>object</code> is the object being
edited and the other fields are object fields being set to a new value</li>
<li>The name <code>Patch</code> is already use to represent a code patch, but the term
<code>Patch</code> works for both cases because they appear in different situations</li>
<li>I'd use <code>Edit</code> etc. but <code>Patch</code> might become standard name on the Fediverse,
coming from the <code>PATCH</code> method of HTTP</li>
<li>Who can create Factories isn't controlled by the OCAP system, but instead
using a setting (but it could also be in the DB) that lists admin usernames</li>
<li>To use a Factory to create actors, you need to have a valid <code>Grant</code> for it,
with the role being at least <code>write</code> (a.k.a developer)</li>
<li>A factory can have both Person collaborators and Teams that have access to
it, so if a Person micro-service creates a Team containing all users, and
that Team is given access to the remote Repository-Factory, users can now
creates repos on the Repository micro-service</li>
</ul>
<h2 id="bonus-synchronized-push">Bonus: Synchronized Push</h2>
<p>In web apps there's often a single big database, and concurrent access to it is
managed using atomic isolated transactions. Actor programming brings a new
approach: Each actor has its own database, its own data. Since actors handle
messages one by one, there's no concurrent access, and transactions happen on
the actor level rather than database level.</p>
<p>But that beautiful model works only when the actor has exclusive access to its
data and database! And that's exactly a problem Vervis had until now:</p>
<ul>
<li>When a human asks to merge a PR by sending an activity to the Repository
actor's inbox, the Repository actor handles the merge</li>
<li>When a human pushes commits via SSH, the SSH server component of Vervis
handles push by directly accessing the repo, without consulting the
Repository actor</li>
</ul>
<p>Thus, we have 2 components that possibly concurrently access the repo! There's
no risk of corruption, thanks to Git locking concurrent access, but there's
difficulty for Repository actor handlers to be atomic, because whenever they
touch the actual Git repo, there's a chance a git-push will concurrently happen
and modify the repo.</p>
<p>To fix that,</p>
<ul>
<li>I made a much-needed upgrade to the actor system (which is going to be the
same system that ForgeFed-v2 a.k.a OcapForge a.k.a Forgely a.k.a
insert-better-name-here will be using!), allowing each actor type to define
its own set of methods</li>
<li>I added a method to the <code>Repository</code> actor, that simply asks it to wait while
the SSH server handles a git-push</li>
</ul>
<p>So technically, the Repository actor doesn't have exclusive write access to the
repo. But whenever a push happens, it now goes through the actor message queue,
which means no concurrent access anymore, and Repository actor message handlers
can now be truly atomic, and easier to implement, knowing no surprising changes
will suddenly appear.</p>
<h2 id="implementation">Implementation</h2>
<ul>
<li>Factory Actor
<ul>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/66870458b704d3959da11eeb237b8276786238f5">Add a new actor type: Factory</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/e196ee6f3460c0c4ab063b2df030ec745f2b83f1">Switch to factory-based creation of Deck, Project and Group</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/df4a2b221e82c4b19b4e94929a24d643e88ee42d">DB, S2S: Factory: Record set of allowed types</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/b74d0d46c4dd9ac15b62de526874b8a848a0987e">S2S: Factory: Implement collaborators and teams</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/94762ca76c31e09ea593d919e20b2a731958fca3">UI, C2S, S2S: Factory: Make allowed types editable</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/a03968ca0bb1cf4eaa7b29d53539f7b6888e9dd3">HomeR, NewR: List grants from extensions, not just direct ones</a></li>
</ul>
</li>
<li>Synchronized Push
<ul>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/cd1bc1aee3d8af403e5b69b57809f78fb0db286b">Pass theater to runSsh</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/ea463703b5805d33622c8745ac0f9c6a6ac22681">Upgrade actor system, now using HList, to allow per-actor method type</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/a74b24f61a0cb190b199a4dd6907c7078f36f6a9">Make push-to-repo SSH events sequential via the Repo actor</a></li>
</ul>
</li>
</ul>
<h2 id="funding">Funding</h2>
<p>I really want to thank NLnet for funding this work! The extended grant is
allowing me to continue backend work, and allowing André to work on the
<a href="https://codeberg.org/Anvil/Anvil">Anvil</a> frontend.</p>
<h2 id="see-it-in-action">See It in Action</h2>
<p>I recorded a little demo of all this! <a href="https://tube.towards.vision/w/3Nz84fkVMsNHUVsNEvAP8V">Watch it on my PeerTube
instance</a>.</p>
<p>If you want to play with things yourself, you can create account(s) on the demo
instances - <a href="https://fig.fr33domlover.site">fig</a>, <a href="https://grape.fr33domlover.site">grape</a>, <a href="https://walnut.fr33domlover.site">walnut</a> - and try the things I've mentioned
and done in the video.</p>
<p>If you encounter any bugs, let me know! Or <a href="https://codeberg.org/ForgeFed/Vervis/issues">open an
issue</a></p>
<h2 id="comments">Comments</h2>
<p>Come chat with us on
<a href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">Matrix</a>!</p>
<p>And we have an account for ForgeFed on the Fediverse:
<a href="https://floss.social/@forgefed">https://floss.social/@forgefed</a></p>
<p>Right after publishing this post, I'll make a toot there to announce the post,
and you can comment there :)</p>


    </main>
    <footer class="footer">
      <p xmlns:dct="http://purl.org/dc/terms/">
        <a rel="license"
           href="http://creativecommons.org/publicdomain/zero/1.0/">
          <img src="https://licensebuttons.net/p/zero/1.0/88x31.png"
               style="border-style: none;"
               alt="CC0" />
        </a>
        <br />
        <a rel="dct:publisher" href="https://forgefed.org/">
          <span property="dct:title">The ForgeFed team</span>
        </a>
        has dedicated all copyright and related and neighboring
        rights to
        <span property="dct:title">ForgeFed</span> to the public domain
        worldwide.
      </p>
      <p>❤ Copying is an act of love. Please copy, reuse and share!</p>
      <p>
        Site generated with
        <a class="footer__link" href="https://www.getzola.org">Zola</a>
        and
        <a class="footer__link" href="https://tabatkins.github.io/bikeshed">Bikeshed</a>.
      </p>
      <p>
        <a href="https://liberapay.com/ForgeFed/donate">
          <img alt="Donate using Liberapay"
              src="https://liberapay.com/assets/widgets/donate.svg" />
        </a>
        <a href="https://opencollective.com/forgefed">
          <img alt="Donate using Open Collective"
              src="/img/open_collective.svg" />
        </a>
      </p>
    </footer>
    <script>
      let html = document.documentElement
      let siteNav = document.getElementById("site-nav")

      function currentColorScheme() {
        if (html.classList.contains("dark")) return "dark"
        if (html.classList.contains("light")) return "light"
        if (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) return "dark"
        return "light"  // default
      }

      let themeSelectionElement = document.createElement("a")
      themeSelectionElement.className = "nav__link"
      themeSelectionElement.id="theme-selector"
      themeSelectionElement.onclick=toggleTheme
      themeSelectionElement.innerText = (currentColorScheme() == "dark") ? "🌞" : "🌑"
      siteNav.appendChild(themeSelectionElement)

      function toggleTheme() {
        if (currentColorScheme() == "light") {
          html.classList.remove("light")
          html.classList.add("dark")
          themeSelectionElement.innerText = "🌞"
          sessionStorage.setItem("theme", "dark")
        } else {
          html.classList.remove("dark")
          html.classList.add("light")
          themeSelectionElement.innerText = "🌑"
          sessionStorage.setItem("theme", "light")
        }
      }
    </script>
  </body>
</html>
