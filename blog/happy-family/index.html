<!doctype html>
<html class="default">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ForgeFed</title>
    <link rel="stylesheet" type="text/css" href="/theme.css" />
    <link rel="icon" href="/img/logo.svg" />
    <script>
      // check whether the default colour scheme has been overridden
      // must be done as early as possible to avoid FOUC
      let theme = sessionStorage.getItem("theme")
      if (theme) document.documentElement.classList.add(theme)
    </script>
    <link rel="alternate" type="application/atom+xml" title="ForgeFed Blog" href="/atom.xml">
  </head>
  <body class="body">
    <header class="header">
      <h1 class="header-title">
        <a class="header-title__link" href="/">
            <img src="/img/logo-with-name.svg" alt="ForgeFed" title="ForgeFed" />
        </a>
      </h1>
      <nav class="nav" id="site-nav">
        <a class="nav__link"
           href="/spec">📓 specification</a>
        <a class="nav__link"
           href="/blog">📜 blog</a>
        <a class="nav__link"
           href="https://codeberg.org/ForgeFed/ForgeFed/issues">🐞 issues</a>
        <a class="nav__link"
           href="https://socialhub.activitypub.rocks/c/software/forgefed/60">🗒️ forum</a>
        <a class="nav__link"
           href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">💬 chat</a>
      </nav>
    </header>
    <main class="main">
      
  <h1 class="main-title">Happy Families - Project Nesting</h1>
  <p>
    2024-04-28 by Pere Lev
  </p>
  <p>As always, my <a href="https://todo.towards.vision/share/lecNDaQoibybOInClIvtXhEIFjChkDpgahQaDlmi/auth?view=kanban">task board</a> is available. I use the Vikunja app to track
my work, it's not a static page and requires JS. And sometimes can be a bit
buggy and needs a reload.</p>
<p>You know what? Let me just show you.</p>
<p><img src="https://forgefed.org/blog/happy-family/task-1.png" alt="Tracking my work on project nesting on Vikunja" /></p>
<p><img src="https://forgefed.org/blog/happy-family/task-2.png" alt="Tracking my work on project nesting on Vikunja" /></p>
<p><img src="https://forgefed.org/blog/happy-family/task-3.png" alt="Tracking my work on project nesting on Vikunja" /></p>
<h2 id="nesting">Nesting</h2>
<p>I haven't used GitLab lately, and I don't use GitHub (other than for examining
its UI). But unless some huge change happened, that I missed, the following
features are probably still there:</p>
<ul>
<li>On GitLab, projects can be nested, a bit like a folder tree</li>
<li>On GitHub, there's the &quot;organization&quot; concept, which can gather multiple
projects and their access rules in one place</li>
</ul>
<p>One of the federated tools ForgeFed provides, to allow projects to remain
organized and efficient as they grow, and to make the familiar nesting
available to forges that want to implement federation, is <strong>project nesting</strong>.</p>
<p>A project, in the ForgeFed sense, is a set of software development tools,
called <strong>components</strong>. The 3 fundamental components ForgeFed currently
describes are Repository (for storing code), Ticket Tracker (for opening
issues) and Patch Tracker (for opening PRs).</p>
<p>Speaking of components - if you could choose the 4th component ForgeFed will
support, what would you choose? Wiki, releases, CI, kanban board... I suppose
it would be one of those? I wonder which one would have the most impact! Let me
know on the Fediverse? :-) I think right now my tendency is towards CI (it
might be the most difficult to implement, but also an extremely useful and
common tool projects use, including ForgeFed itself, whose website (this
website you're looking at right now) is deployed using Codeberg's CI).</p>
<p>Thanks to federation, project nesting is quite flexible:</p>
<ul>
<li>Project <em>A</em> can become a parent of another project <em>B</em></li>
<li><em>A</em> and <em>B</em> can reside on different servers, and it just works</li>
<li>A project can have more than one parent!</li>
<li>In other words, it's not a folder tree, but more like a directed graph</li>
<li>There's a mechanism for preventing cycles, don't worry (but if you feel like
testing it, see below for live demo links!)</li>
</ul>
<p>I'm really curious if this flexibility can, when the time comes, support a
global decentralized network of forges, where projects can belong to multiple
contexts simultanously, and where federated access control and collaboration
are done really smoothly.</p>
<h2 id="nesting-and-access-control">Nesting and Access Control</h2>
<p>Like on GitLab and GitHub, gathering projects together under higher-level
projects isn't just for visual convenience, it's also a powerful tool for
managing <strong>access control</strong>. If Alice obtains access to a project <em>CoolApp</em>,
she now automatically gains access to all the components of <em>CoolApp</em>, and to
the child projects of <em>CoolApp</em>, and to their own components and child
projects, and so on.</p>
<p>So, when people or teams are given responsibility on a certain group of
projects, an easy way to manage access is to put those projects under one big
parent project, and assign access through this parent project.</p>
<p>How does this automatic nested access control work?</p>
<p>Like almost everything around here, it uses ForgeFed's federated Object
Capability system! Here's an example:</p>
<ul>
<li>I create a project <em>A</em></li>
<li>I now receive a <em>Grant</em> activity, giving me access to <em>A</em></li>
<li>I create a project <em>B</em></li>
<li>I now receive a <em>Grant</em> activity, giving me access to <em>B</em></li>
<li>I make project <em>B</em> a child of project <em>A</em></li>
<li>Project <em>B</em> now passes a special <em>Grant</em> activity to <em>A</em>, and <em>A</em> passes it
to me</li>
</ul>
<p>Now I have 2 authorization paths for manipulating project <em>B</em>:</p>
<ul>
<li>The <em>Grant</em> that <em>B</em> gave me directly when I created it</li>
<li>The <em>Grant</em>-for-access-<em>B</em> that <em>B</em> passed to <em>A</em>, and <em>A</em> passed to me
because of my access to <em>A</em></li>
</ul>
<p>Is this confusing already? The story continues:</p>
<ul>
<li>I now create a ticket tracker <em>TT</em></li>
<li>I now receive a <em>Grant</em> activity, giving me access to <em>TT</em></li>
<li>I now add <em>TT</em> to be a component of project <em>B</em></li>
</ul>
<p>What happens now?</p>
<ul>
<li><em>TT</em> passes a <em>Grant</em> to <em>B</em></li>
<li><em>B</em> extends it to me, due to my direct access to <em>B</em></li>
<li><em>B</em> also extends it to <em>A</em>, its parent project</li>
<li><em>A</em> further extends it to me, due to my direct access to <em>A</em></li>
</ul>
<p>I now have 3 <em>Grant</em>s for accessing <em>TT</em>!</p>
<ul>
<li>The direct one I got when I just created <em>TT</em></li>
<li>The one I got through <em>B</em></li>
<li>The one I got through <em>A</em></li>
</ul>
<p>It's now possible to <em>remove</em> me from having direct access to <em>B</em> and to <em>TT</em>,
and I'll still be able to manipulate them, because project <em>A</em> is extending me
access-to-them.</p>
<p>Here's my (somewhat cryptic) project overview on the <a href="https://grape.fr33domlover.site">grape</a> demo instance.
The items on the list are projects I have access to, and the sublists represent
<em>Grant</em>s that these projects have extended to me.</p>
<p>Items starting with <code>$</code> are projects. Items starting with <code>=</code> are ticket
trackers.</p>
<p><img src="https://forgefed.org/blog/happy-family/your-projects.png" alt="Example of projects and Grants extended through them" /></p>
<p>Another way to observe my Grants is to browse to some resource, such as a
ticket tracker or project or team, and if I have any Grants to access it, a &quot;My
access&quot; section will appear, like this:</p>
<p><img src="https://forgefed.org/blog/happy-family/my-access.png" alt="Example of my access to a ticket tracker" /></p>
<p>So we're looking at this ticket tracker <em>TTT</em>, to which I have both direct
access (which in this case I got when I created it), and I have another Grant
that I received via some project <em>CCC</em>, because <em>TTT</em> is a component of <em>CCC</em>
and I'm a direct collaborator in <em>CCC</em>.</p>
<p>What's special about both views above is that their data is tracked by the
Person actor, not by the projects. It really makes things more convenient,
especially as more and more pieces are implemented and the system is getting
complex. Obviously this whole UI is just PoC-level (while we're working on an
actual frontend application). But I hope you'll see in the demo, how much
easier it makes things. Compared to the bare-bones forms and JSON objects I've
been playing with until now, this is a major convenience upgrade :P</p>
<p>I implemented those UIs primarily in the following commits:</p>
<ul>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/acdce58fc614397bb5a9370b130cd53b73f06711">DB: Add a Resource table, and use it in all local Actors except Person</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/48811545797a84a0bd79b6d3bbab7d415d0c13f3">DB: Permit: Use Resource instead of Actor in extension resource</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/888a30e989d0e2bba8ba2201b9f682b417d75a1a">DB: Switch Collab and Permit to use Resource</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/dae57c394da2f74101703a3af423a29403c93a3c">UI: Personal Overview: Display info of received permits, not just the Grant URI</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/ab08e593efa3f2e9e98673c36badd93b952b2a4d">S2S: Person: Grant: Record role and resource in Permit record</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/fc9d56dd34369b2f6bfe62a3dbf6c58b77f47f7a">UI: When logged in, display my delegated access to a given local resource</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/07d9f9adabe511357eeb707896d842df6521d8dc">UI: In my-grants-for-given-resource display, list direct grants as well</a></li>
</ul>
<h2 id="nesting-implementation">Nesting implementation</h2>
<p>By far the most complicated part of the implementation is the actual logic,
i.e. the Server-to-server (S2S) activity handlers that implement the activity
sequences that create and remove the links between children and parents. The
child-parent link, perhaps like in real life, is the most complex piece of the
OCAP system that I've implemented so far.</p>
<p>Here's the update to the specification, which describes both linking and
unlinking of parents and children, both for projects and for teams:</p>
<ul>
<li><a href="https://codeberg.org/ForgeFed/ForgeFed/pulls/217">Spec: Describe adding/removing parents/children to/from teams and projects</a></li>
</ul>
<p>The process of forming a child-parent link involves several activity types:
Add, Accept and Grant. And there are 4 different modes:</p>
<ul>
<li>Child-active: I'm getting a new child, and I'm initiating the process,
waiting for the child to approve</li>
<li>Child-passive: I'm getting a new child, but the child is the one initiating,
now waiting for me to approve</li>
<li>Parent-active: I'm getting a new parent, and I'm initiating the process,
waiting for the parent to approve</li>
<li>Parent-passive: I'm getting a new parent, but the parent is the one
initiating, now waiting for me to approve</li>
</ul>
<p>If I'm in child-active mode, then the child is in parent-passive. And if I'm in
child-passive mode, then the child is in parent-active. So these modes must
really dance together. Commits:</p>
<ul>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/a1df4b3bdbb688ae22a009a78b838d8469ffa545">Vocab: Expand Remove activity parsing in preparation for child/parent mode</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/ab786eb67cafd0061936eb2c161f96a94cd8a34d">UI, Vocab: Link from project/team to children &amp; parents pages</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/1f06d689f58d7e5133f59e89c43acd36165edba6">DB tables for project/team parent/child tracking</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/bce8079cb5f3b8a328561a19bf4e470c999c7bef">S2S: Project: Add: Handle adding a child/parent; also update C2S Add</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/bdce87cf766a768df19a39bf4018722c49149aa4">S2S: Project: Accept: Implement child/parent mode</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/3570d502cbe53a7c5066ed6866b8f563cc5f3156">S2S: Project: Grant: Child/parent delegation when adding collab/component</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/1d13d7a5513c038228ba78f6fe3b196b8bf13ca5">S2S: Project: Grant: Implement child mode</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/1c10d3fb0394f21e48b422fd1e6efefd96b6171a">S2S: Project: Grant: Implement parent mode</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/692c34bdec16b2c4c0a75b9fad0187bd15bcccab">S2S: Project: Grant: When getting a new parent, send them a start-Grant</a></li>
</ul>
<p>It was a huge piece of work! But it's just the logic, without UI around it, so
it was still invisible.</p>
<h2 id="loop-prevention">Loop prevention</h2>
<p>What if:</p>
<ul>
<li>Project A becomes a parent of project B</li>
<li>Project B becomes a parent of project C</li>
<li>Project C now becomes a parent of project... A?</li>
</ul>
<p>Actually, the circular relationship itself isn't the problem. The problem is
that once it is formed, these project would start to send and extend Grants.
And if they aren't careful, they might end up extendind and extending the same
Grant chain infinitely, getting their CPUs and DBs and network connections busy
with this empty work. Also known as an accidental (or intentional) DDOS attack.
How to prevent this?</p>
<p>My first naive thought was: If I'm a project and about to add a child/parent,
What if I loop over the children/parents, and recursively their own
children/parents, to make sure I don't find myself somewhere in there? If I do,
it's a sign I can't add the child/parent, overwise I'd create a circular
situation. Thoughts?</p>
<p>This <em>might</em> work, but:</p>
<ul>
<li>It's very wasteful because of how it fans out, and can very quickly end up
having to traverse tons of projects</li>
<li>It's not really effective at preventine a DDOS attack, because regardless of
who a project <em>claims</em> its children and parents are, it might make entirely
different choices where to extend the Grant chains</li>
<li>In other words, the chain is what matters here, not the public data of the
parent-child links (i.e. a malicious of buggy project's talk and walk might
differ)</li>
</ul>
<p>So I went for a different approach, using the Grant chain itself:</p>
<ul>
<li>When I'm about to extend a chain, traverse the chain back to the beginning,
making sure I don't appear there</li>
</ul>
<p>Since the chain is a list, there's no fanning out! In computer science speak,
while the naive approach was <em>O(n)</em>, the new one is <em>O(log n)</em>, where <em>n</em> is
the amount of projects in the project graph.</p>
<p>But naively traversing the chain also creates a risk of DDOS: What if a
malicious (or buggy) project sends me a really, really long chain? Like, with a
million items?</p>
<p>The chain length corresponds to the number of Project nesting levels. While
theoretically it's arbitary, in practice even the biggest projects probably
need just a few levels of nesting. I expect that 4 is really enough for nearly
all projects, even the bigger ones. But perhaps really big projects need a bit
more. Since the numbers are so small, why don't we give them a comfortable
margin, just in case?</p>
<p>So, I added a new settings in the Vervis settings file: The maximal depth that
a Grant chain is allowed to have. A chain bigger than this is immediately
considered invalid. I set the default limit to 16, which is also what the 3
demo instances are using. But even if it was 100, which is probably more than
anyone out there ever needs, it's still way way less than a million.</p>
<p>So, this is the mechanism that prevents loops:</p>
<ol>
<li>Traverse the chain before extending</li>
<li>Verify the length is below the maximum (default is 16)</li>
<li>Verify none of the Grants in the chain is mine</li>
</ol>
<p>Commits:</p>
<ul>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/6dd6dc17e532f611ac3d9e42e0dcaa8125a3e623">S2S: Enforce max chain length when verifying OCAPs</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/789411f5d2f9df0097fc7527978251e15ff89fba">S2S: Project: Grant: Before extending a Grant, test to avoid infinite loop</a></li>
</ul>
<h2 id="basic-ui">Basic UI</h2>
<p>So far, all of this is logic behind the scenes. I started adding the most basic
UI, forms that require copying and pasting URIs. I always add them anyway,
because I need them for flexibly testing federated interactions, but I usually
add the nice buttons first. This time, I started with the forms, so that the
behind-the-scenes mechanisms are visible, before I cover them with more
convenient UI.</p>
<p>I also added UI for viewing parents and children of a project.</p>
<ul>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/802df6b15b43c61a01157ccc2772b83a23dc3543">UI, AP: Display project and team children and parents</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/d32da785b85ebde791eb8323148c93be1c7a1167">UI: Project: Children: Display child invites, their details &amp; status</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/b80d897e0c8037ebc35a76f6eb190c3e20de3022">UI: Project: Parents: Display parent invite info &amp; status</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/992e17f1ca5f746f97082c718ee6df40c5a01d7c">UI, Client: Forms for adding and accepting a parent or child</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/0b08a7692ae1bc290e10d0cab0568b864326a4c2">UI: Browse: List project's enabled children</a></li>
</ul>
<p>Viewing per project:</p>
<p><img src="https://forgefed.org/blog/happy-family/project-children.png" alt="A project's children list, listing 2 children" /></p>
<p>Viewing projects on an instance:</p>
<p><img src="https://forgefed.org/blog/happy-family/project-browser.png" alt="Example of project nesting" /></p>
<p>Adding a child-parent link:</p>
<p><img src="https://forgefed.org/blog/happy-family/add-form.png" alt="Empty form for adding a child project" /></p>
<p>I tested my implementation using this UI, both local and federated. Copying and
pasting URIs is of course quite annoying, but it allows to see how things work
behind the scenes. See video demo below.</p>
<h2 id="link-removal">Link removal</h2>
<p>Since Vervis has been mostly in PoC stage, and since the code evolves very
quickly and it's nice to have flexibility, and since I'm me, so far I've mostly
focused on code that <em>creates</em> stuff, and much less on code that <em>deletes</em>
stuff. I think I never properly implemented things like deleting a user
account, or deleting a ticket tracker. I mean, I did at some point, but then
big changes to the code broke those features, and it was really difficult to
maintain them, especially considering the low priority.</p>
<p>But now that the Anvil frontend is on its way, and Vervis is getting more and
more core features implemented, I really hope to catch up and make it possible
to delete things properly, as well as edit, and at some point also freely and
easily migrate data between servers (for which I hope to use the F3
specification, on which my Forge Federation colleagues Earl Warren and Loic
Dachary have been working for some time now, and I'm excited to integrate with
that).</p>
<p>The child-parent link removal process was much easier to implement than the
link creation process, but it simiarly involves multiple types of activities:
Remove, Accept, Revoke.</p>
<p>Commits that implement the logic:</p>
<ul>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/048c429def90ef29bf0ddc1b0a63c50d087df282">S2S: Project: Remove: Implement child/parent mode</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/223fbf3d0eb928ecbb0967157ca2e98563983df5">S2S: Project: Accept: Implement remove-child mode</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/a16fb6cd193e6137aa8ee8ac63b3bca84bb6b59a">S2S: Project: Revoke: Handle parent revoking the delegator-Grant</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/24aba4d370487b24a2cb89ddd3218e3eae640aa8">S2S: Project: Revoke: Handle child revoking one of the Grants they sent me</a></li>
</ul>
<p>And UI, a form similar to the ones above:</p>
<ul>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/1f36657084cc3ace783ecaa6df423a68eee50b23">UI: Removal form: Extend to work on children/parents as well</a></li>
</ul>
<h2 id="more-convenient-ui">More convenient UI</h2>
<p>With all those pieces in place, I really wanted to feel the ease of just
clicking stuff without having to manually choose which Grant activity to use
for authorization, and pasting around all these URIs.</p>
<p>What if I browse to project A, and there's a little box where I just paste the
URI of project B (since there's no JS-powered search yet, that's the minimum to
paste) and click &quot;Add&quot;? And then the other side approves the child-parent link
using a button, without any pasting? And what if parent/child removal is a
single button click too?</p>
<p>So, I decided to implement these things:</p>
<ul>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/abefcbd310031d4fdb5acfbc1f7578e0943855aa">UI: Project: Provide buttons for removing children</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/bb1685f695f051aeefb8176d53bb3c7b42e7018c">UI: Project: Buttons for removing parents</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/d9d6b9fcedbd2022fe0118fec6f0fa66e2cda28e">UI: Project: Children: Form for adding a child</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/082eae7a51e411a911ff216da11957be2b46f5dd">UI: Project: Children: Button for approving the Add</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/99f6d950a203e00a073e039b764c011fdaa6eec0">UI: Project: Parents: Form for adding a parent by URI</a></li>
<li><a href="https://codeberg.org/ForgeFed/Vervis/commit/ef036fd08b3ba4ea0a64d999baddc866b14ed9ae">UI: Project: Parents: Button for approving a parent</a></li>
</ul>
<p>Adding a child:</p>
<p><img src="https://forgefed.org/blog/happy-family/add-child.png" alt="Adding a child project by URI" /></p>
<p>Now the project displays the invited child:</p>
<p><img src="https://forgefed.org/blog/happy-family/see-invite.png" alt="Child invite" /></p>
<p>If we browse to the child's parents page, we can see the invite there too, and
since I have admin access to the child, there's an <em>Approve</em> button:</p>
<p><img src="https://forgefed.org/blog/happy-family/see-invite-other-side.png" alt="Same invite, from the child's perspective" /></p>
<p>Voila! The child-parent link is now enabled, and there's a <em>Remove</em> button to
disable it:</p>
<p><img src="https://forgefed.org/blog/happy-family/child-enabled.png" alt="Child is now enabled" /></p>
<h2 id="funding">Funding</h2>
<p>I really want to thank NLnet for funding this work, and much more to come! My
grant (which just expired) is going to be extended, and
<a href="https://jaenis.ch">André</a>'s work on the Anvil frontend is going to be funded
too.</p>
<h2 id="see-it-in-action">See It in Action</h2>
<p>I recorded a little demo of all this! <a href="https://tube.towards.vision/w/7q5UdESQatoWYKvG1xeSAo">Watch it on my PeerTube
instance</a>.</p>
<p>If you want to play with things yourself, you can create account(s) on the demo
instances - <a href="https://fig.fr33domlover.site">fig</a>, <a href="https://grape.fr33domlover.site">grape</a>, <a href="https://walnut.fr33domlover.site">walnut</a> - and try the things I've mentioned
and done in the video.</p>
<p>If you encounter any bugs, let me know! Or <a href="https://codeberg.org/ForgeFed/Vervis/issues">open an
issue</a></p>
<h2 id="comments">Comments</h2>
<p>Come chat with us on
<a href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">Matrix</a>!</p>
<p>And we have an account for ForgeFed on the Fediverse:
<a href="https://floss.social/@forgefed">https://floss.social/@forgefed</a></p>
<p>Right after publishing this post, I'll make a toot there to announce the post,
and you can comment there :)</p>


    </main>
    <footer class="footer">
      <p xmlns:dct="http://purl.org/dc/terms/">
        <a rel="license"
           href="http://creativecommons.org/publicdomain/zero/1.0/">
          <img src="https://licensebuttons.net/p/zero/1.0/88x31.png"
               style="border-style: none;"
               alt="CC0" />
        </a>
        <br />
        <a rel="dct:publisher" href="https://forgefed.org/">
          <span property="dct:title">The ForgeFed team</span>
        </a>
        has dedicated all copyright and related and neighboring
        rights to
        <span property="dct:title">ForgeFed</span> to the public domain
        worldwide.
      </p>
      <p>❤ Copying is an act of love. Please copy, reuse and share!</p>
      <p>
        Site generated with
        <a class="footer__link" href="https://www.getzola.org">Zola</a>
        and
        <a class="footer__link" href="https://tabatkins.github.io/bikeshed">Bikeshed</a>.
      </p>
      <p>
        <a href="https://liberapay.com/ForgeFed/donate">
          <img alt="Donate using Liberapay"
              src="https://liberapay.com/assets/widgets/donate.svg" />
        </a>
        <a href="https://opencollective.com/forgefed">
          <img alt="Donate using Open Collective"
              src="/img/open_collective.svg" />
        </a>
      </p>
    </footer>
    <script>
      let html = document.documentElement
      let siteNav = document.getElementById("site-nav")

      function currentColorScheme() {
        if (html.classList.contains("dark")) return "dark"
        if (html.classList.contains("light")) return "light"
        if (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) return "dark"
        return "light"  // default
      }

      let themeSelectionElement = document.createElement("a")
      themeSelectionElement.className = "nav__link"
      themeSelectionElement.id="theme-selector"
      themeSelectionElement.onclick=toggleTheme
      themeSelectionElement.innerText = (currentColorScheme() == "dark") ? "🌞" : "🌑"
      siteNav.appendChild(themeSelectionElement)

      function toggleTheme() {
        if (currentColorScheme() == "light") {
          html.classList.remove("light")
          html.classList.add("dark")
          themeSelectionElement.innerText = "🌞"
          sessionStorage.setItem("theme", "dark")
        } else {
          html.classList.remove("dark")
          html.classList.add("light")
          themeSelectionElement.innerText = "🌑"
          sessionStorage.setItem("theme", "light")
        }
      }
    </script>
  </body>
</html>
