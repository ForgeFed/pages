<!doctype html>
<html class="default">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ForgeFed</title>
    <link rel="stylesheet" type="text/css" href="/theme.css" />
    <link rel="icon" href="/img/logo.svg" />
    <script>
      // check whether the default colour scheme has been overridden
      // must be done as early as possible to avoid FOUC
      let theme = sessionStorage.getItem("theme")
      if (theme) document.documentElement.classList.add(theme)
    </script>
    <link rel="alternate" type="application/atom+xml" title="ForgeFed Blog" href="/atom.xml">
  </head>
  <body class="body">
    <header class="header">
      <h1 class="header-title">
        <a class="header-title__link" href="/">
            <img src="/img/logo-with-name.svg" alt="ForgeFed" title="ForgeFed" />
        </a>
      </h1>
      <nav class="nav" id="site-nav">
        <a class="nav__link"
           href="/spec">📓 specification</a>
        <a class="nav__link"
           href="/blog">📜 blog</a>
        <a class="nav__link"
           href="https://codeberg.org/ForgeFed/ForgeFed/issues">🐞 issues</a>
        <a class="nav__link"
           href="https://socialhub.activitypub.rocks/c/software/forgefed/60">🗒️ forum</a>
        <a class="nav__link"
           href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">💬 chat</a>
      </nav>
    </header>
    <main class="main">
      
  <p>ForgeFed is a <strong>federation protocol for software forges</strong> and code collaboration
tools for the software development lifecycle and ecosystem. This includes
repository hosting websites, issue trackers, code review applications, and more.
ForgeFed provides a common substrate for people to create interoperable code
collaboration websites and applications.</p>
<p><strong>Federation</strong> means that these websites can interact, allowing the humans
using them to interact too, despite being registered on different websites. For
example, imagine you could host your Git repos anywhere you want, perhaps even
your own personal website, but still be able to open issues and submit pull
requests against other people's repos hosted elsewhere, without having to
create accounts on those other websites!</p>
<p>Without federation, we end up having to choose between:</p>
<ul>
<li>Centralizing into huge profit-oriented websites, where we're powerless</li>
<li>Hosting our code on a small website where we're in control and freedom but
isolated from the community</li>
</ul>
<p>With federation, all the websites now communicate with each other to form
<strong>a network and community of collaboration</strong> in which we're all both free and
connected. It puts the power back into our hands to create tools and
collaborate in ways that are aligned with human needs, powerful and safe ways
that allow us to include everyone and that don't depend on some big company's
policies or some website suddenly shutting down. Let's create the future
together!</p>
<h2 id="how-does-it-work">How does it work?</h2>
<p>ForgeFed is an <a href="https://www.w3.org/TR/activitypub/">ActivityPub</a> extension. ActivityPub is an actor-model based
protocol for federation of web services and applications.</p>
<p>It's a bit like e-mail, except the data sent is JSON objects (i.e. structured
computer-readable data), and not only humans have inboxes where they can be
contacted, but also repositories and issue trackers have inboxes through which
they can be remotely and safely interacted with.</p>
<p>On top of ActivityPub's vocabulary (common language for websites to use for
communicating) and protocol, ForgeFed defines new vocabulary terms related to
repositories, commits, patches, issues, etc. and the protocol for creating and
interacting with such objects across servers.</p>
<p>You can find more technical details in our <a href="https://codeberg.org/ForgeFed/ForgeFed">repository</a>.</p>
<h2 id="project-status">Project status</h2>
<p>The best way to keep track of our progress is to follow us on the <a href="https://floss.social/@forgefed">Fediverse</a>.
You can also join our chat using <a href="https://matrix.to/#/#forgefed:libera.chat">Matrix</a> or Libera.Chat at #forgefed.</p>
<h2 id="implementations">Implementations</h2>
<ul>
<li><a href="https://vervis.peers.community/">Vervis</a> is the reference implementation of ForgeFed. It serves as a demo
platform for testing the protocol and new features.</li>
<li><a href="https://forgejo.org">Forgejo</a> is implementing federation.</li>
<li>Pagure has an unmaintained <a href="https://pagure.io/forge-fed/forge-fed">ForgeFed plugin</a>.</li>
</ul>


    </main>
    <footer class="footer">
      <p xmlns:dct="http://purl.org/dc/terms/">
        <a rel="license"
           href="http://creativecommons.org/publicdomain/zero/1.0/">
          <img src="https://licensebuttons.net/p/zero/1.0/88x31.png"
               style="border-style: none;"
               alt="CC0" />
        </a>
        <br />
        <a rel="dct:publisher" href="https://forgefed.org/">
          <span property="dct:title">The ForgeFed team</span>
        </a>
        has dedicated all copyright and related and neighboring
        rights to
        <span property="dct:title">ForgeFed</span> to the public domain
        worldwide.
      </p>
      <p>❤ Copying is an act of love. Please copy, reuse and share!</p>
      <p>
        Site generated with
        <a class="footer__link" href="https://www.getzola.org">Zola</a>
        and
        <a class="footer__link" href="https://tabatkins.github.io/bikeshed">Bikeshed</a>.
      </p>
      <p>
        <a href="https://liberapay.com/ForgeFed/donate">
          <img alt="Donate using Liberapay"
              src="https://liberapay.com/assets/widgets/donate.svg" />
        </a>
        <a href="https://opencollective.com/forgefed">
          <img alt="Donate using Open Collective"
              src="/img/open_collective.svg" />
        </a>
      </p>
    </footer>
    <script>
      let html = document.documentElement
      let siteNav = document.getElementById("site-nav")

      function currentColorScheme() {
        if (html.classList.contains("dark")) return "dark"
        if (html.classList.contains("light")) return "light"
        if (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) return "dark"
        return "light"  // default
      }

      let themeSelectionElement = document.createElement("a")
      themeSelectionElement.className = "nav__link"
      themeSelectionElement.id="theme-selector"
      themeSelectionElement.onclick=toggleTheme
      themeSelectionElement.innerText = (currentColorScheme() == "dark") ? "🌞" : "🌑"
      siteNav.appendChild(themeSelectionElement)

      function toggleTheme() {
        if (currentColorScheme() == "light") {
          html.classList.remove("light")
          html.classList.add("dark")
          themeSelectionElement.innerText = "🌞"
          sessionStorage.setItem("theme", "dark")
        } else {
          html.classList.remove("dark")
          html.classList.add("light")
          themeSelectionElement.innerText = "🌑"
          sessionStorage.setItem("theme", "light")
        }
      }
    </script>
  </body>
</html>
